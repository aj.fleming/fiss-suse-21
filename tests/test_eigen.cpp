#include <gtest/gtest.h>
#include <cmath>
#include <complex>

#include "linear.hpp"
#include "eigen.hpp"

using namespace fiss;

class EigenvalueMethodTests : public ::testing::Test
{
protected:
    const double TOL = 1.0e-8;
    Matrix<double> *m1;
    Matrix<double> *diag1;
    Matrix<double> *lowTri1;
    TridiagMatrix<double> *tridiag1;
    void SetUp()
    {
        m1 = new Matrix<double>(2,2);
        (*m1)[{0,0}] = 0;
        (*m1)[{0,1}] = 1;
        (*m1)[{1,0}] = -2;
        (*m1)[{1,1}] = -3;
        diag1 = new Matrix<double>(4, 4);
        for (size_t i = 0; i < 4; i++)
        {
            (*diag1)[{i, i}] = 6 - i;
        }
        lowTri1 = new Matrix<double>(3,3);
        (*lowTri1)[{0,0}] = 1;
        (*lowTri1)[{1,0}] = 1;
        (*lowTri1)[{1,1}] = 2;
        (*lowTri1)[{2,0}] = 2;
        (*lowTri1)[{2,1}] = 3;
        (*lowTri1)[{2,2}] = 3;
        Vector<double> a(10), b(9), c(9);
        fillVector(a, 8.0);
        fillVector(b, 2.0);
        fillVector(c, 2.0);
        tridiag1 = new TridiagMatrix<double>(b,a,c);
    };
    void TearDown()
    {
        delete m1,diag1,lowTri1,tridiag1;
    };
};

/*
TEST_F(EigenvalueMethodTests, PowerIterationGeneral){
    double max_eval = eigenval::findLargestByPowerIteration(*diag1, TOL);
    EXPECT_NEAR(6, max_eval, 1.0e-6);
    max_eval = eigenval::findLargestByPowerIteration(*lowTri1, TOL);
    EXPECT_NEAR(3, max_eval, 1.0e-6);
    max_eval = eigenval::findLargestByPowerIteration(*m1, TOL);
    EXPECT_NEAR(-2, max_eval, 1.0e-6);
    max_eval = eigenval::findLargestByPowerIteration(*tridiag1, TOL);
    EXPECT_NEAR(11.83797189, max_eval, 1.0e-4);
};
*/

TEST_F(EigenvalueMethodTests, PowerIterationOnSymmetric)
{
    
};

TEST_F(EigenvalueMethodTests, LanczosIterationGeneral)
{
    
};