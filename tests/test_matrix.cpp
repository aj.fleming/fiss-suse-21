#include <gtest/gtest.h>
#include <cmath>
#include <complex>
#include <tuple>

#include "linear.hpp"

using namespace fiss;

class MatrixOperationTests : public ::testing::Test
{
protected:
    Matrix<double> *m1, *m2, *m3, *m4;
    void SetUp(){
        m1 = new Matrix<double>(8,8);
        fillMatrix(*m1, 2.0);
        m2 = new Matrix<double>(8,8);
        m3 = new Matrix<double>(8,8);
        m4 = new Matrix<double>(16,16);

    };
    void TearDown(){
        delete m1, m2, m3, m4;
    };
};

TEST_F(MatrixOperationTests, GetMatrixColumn){
    VectorView<double> test_1(m1->get_col(0));
    EXPECT_EQ(test_1.size(), std::get<0>(m1->shape()));
    VectorView<double> test_1a(m1->get_col(2));
    EXPECT_EQ(test_1a.size(), std::get<0>(m1->shape()));
};

TEST_F(MatrixOperationTests, MatrixMatrixAddition)
{
    
};

TEST_F(MatrixOperationTests, MatrixMatrixSubtraction)
{
    
};

TEST_F(MatrixOperationTests, CompoundMatrixExpression)
{
    
};

TEST_F(MatrixOperationTests, MatrixMatrixContraction)
{
  
};

TEST_F(MatrixOperationTests, MatrixVectorProduct)
{
    
};

TEST_F(MatrixOperationTests, TridiagVectorProduct)
{
    Vector<double> a(8), b(7), c(8), d(8);
    fillVector<double>(a, 4);
    fillVector(b, 2.0);
    fillVector(c, 1.0);

    for(int i = 0; i<d.size(); i++){
        d[i] = i+1;
    }

    TridiagMatrix<double> test_1(b,a,b);
    EXPECT_EQ(std::get<0>(test_1.shape()), 8);
    Vector<double> res(test_1*c);
    EXPECT_EQ(res[0], 6);
    for(int i = 1; i<7; i++){
        EXPECT_EQ(res[i], 8);
    }
    EXPECT_EQ(res[7], 6);

    Vector<double> res_b(test_1*d);
    EXPECT_EQ(res_b[0], 8);
    for(int i = 1; i<7; i++){
        EXPECT_EQ(res_b[i], 16+8*(i-1));
    }
    EXPECT_EQ(res_b[7], 46);
};