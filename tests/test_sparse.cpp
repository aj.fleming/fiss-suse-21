#include <gtest/gtest.h>
#include <cmath>
#include <complex>
#include <string>

#include "linear.hpp"

using namespace fiss;

class SparseMatrixTests : public ::testing::Test
{
protected:
    SparseMatrix<double> *sym1, *lowTri1;
    void SetUp(){
        size_t *row_starts = new size_t[4]{0, 1, 2, 4};
        size_t *cols = new size_t[4]{0,1,0,2};
        double *data = new double[4]{1,2,4,3};

        sym1 = new SparseMatrix<double>({3,3},4, row_starts, cols, data, true);

        row_starts = new size_t[4]{0, 1, 2, 4};
        cols = new size_t[4]{0,1,0,2};
        data = new double[4]{1,2,4,3};

        lowTri1 = new SparseMatrix<double>({3,3},4, row_starts, cols, data, false);
    };
    void TearDown(){
        delete sym1, lowTri1;
    };
};

TEST_F(SparseMatrixTests, SparseMatrixVectorProduct)
{
    Vector<double> t1(3);
    for(int i = 0; i<t1.size(); i++){
        t1[i] = 5+i;
    }
    Vector<double> result((*lowTri1) * t1);
    EXPECT_DOUBLE_EQ(result[0], 5);
    EXPECT_DOUBLE_EQ(result[1], 12);
    EXPECT_DOUBLE_EQ(result[2], 41);
};

TEST_F(SparseMatrixTests, SparseSymmetricVectorProduct){
    Vector<double> t1(3);
    for(int i = 0; i<t1.size(); i++){
        t1[i] = 5+i;
    }
    Vector<double> result((*sym1) * t1);
    EXPECT_DOUBLE_EQ(result[0], 33);
    EXPECT_DOUBLE_EQ(result[1], 12);
    EXPECT_DOUBLE_EQ(result[2], 41);
};

TEST_F(SparseMatrixTests, LoadMatrixToCSR)
{
    FAIL();
};

TEST_F(SparseMatrixTests, LoadSymmetricMatrixToCSR){
    FAIL();
}; 