#include <gtest/gtest.h>
#include <cmath>
#include <complex>

#include "linear.hpp"

using namespace fiss;

class VectorOperationTests : public ::testing::Test
{
protected:
    Vector<int> *v1_int, *v2_int, *v3_int;
    Vector<double> *v1_double, *v2_double;
    Vector<double> *v1_double_big, *v2_double_big;
    Vector<cdouble> *v1_complex;

    void SetUp()
    {
        v1_int = new Vector<int>(8);
        v2_int = new Vector<int>(8);
        v3_int = new Vector<int>(8);
        fillVector(*v1_int, 1);
        fillVector(*v2_int, 2);
        fillVector(*v3_int, 3);

        v1_double = new Vector<double>(8);
        v2_double = new Vector<double>(8);
        v1_double_big = new Vector<double>(32);
        v2_double_big = new Vector<double>(32);

        for (size_t i = 0; i < 8; i++)
        {
            (*v1_double)[i] = i;
            (*v2_double)[i] = 2 * i;
        }
        fillVector(*v1_double_big, 1.5);
        fillVector(*v2_double_big, 0.75);

        v1_complex = new Vector<cdouble>(8);
        fillVector(*v1_complex, {0, 1});
    };

    void TearDown()
    {
        delete v1_int;
        delete v2_int;
        delete v3_int;
        delete v1_double;
        delete v1_complex;
        delete v1_double_big;
        delete v2_double_big;
    };
};

TEST_F(VectorOperationTests, VectorVectorAddition)
{
    // verify that 1+2 = 3
    auto test_1 = (*v1_int) + (*v2_int);
    ASSERT_NE(&test_1, nullptr);
    EXPECT_EQ(test_1.size(), v1_int->size());
    EXPECT_EQ(test_1.size(), v2_int->size());
    for (size_t i = 0; i < test_1.size(); i++)
    {
        EXPECT_EQ(3, test_1[i]);
    }

    // verify that 1 + (0+i) = 1+i
    auto test_2 = (*v1_double) + (*v1_complex);
    ASSERT_NE(&test_2, nullptr);
    EXPECT_EQ(test_2.size(), v1_complex->size());
    for (size_t i = 0; i < test_2.size(); i++)
    {
        cdouble expect_result = {i, 1};
        EXPECT_EQ(expect_result, test_2[i]);
    }

    // verify that one cannot add vectors of different sizes
    EXPECT_THROW(auto dim_mismatch_test = (*v1_double) + (*v1_double_big);, std::invalid_argument);
};

TEST_F(VectorOperationTests, VectorScalarMult)
{

    // verify correct result for pass-by-lvalue
    auto test_correct_a = (*v1_double) * 2.0;
    // verify correct result for pass-by-rvalue
    auto test_correct_b = Vector<double>(*v1_double) * 2.0;
    // verify commutation
    auto test_correct_c = 2.0 * (*v1_double);
    auto test_correct_d = 2.0 * Vector<double>(*v1_double);
    ASSERT_NE(&test_correct_a, nullptr);
    for (size_t i = 0; i < test_correct_a.size(); i++)
    {
        EXPECT_DOUBLE_EQ(test_correct_a[i], 2 * (*v1_double)[i]);
        EXPECT_DOUBLE_EQ(test_correct_a[i], 2 * (*v1_double)[i]);
        EXPECT_DOUBLE_EQ(test_correct_a[i], test_correct_c[i]);
        EXPECT_DOUBLE_EQ(test_correct_a[i], test_correct_d[i]);
    }

    // verify construction of vector from expression tree
    Vector<double> test_copy_a(test_correct_a);
    ASSERT_NE(&test_copy_a, nullptr);
    for (size_t i = 0; i < test_copy_a.size(); i++)
    {
        EXPECT_DOUBLE_EQ(test_copy_a[i], 2 * (*v1_double)[i]);
    }

    // verify inverse operation
    auto test_2 = 0.5 * (*v1_double);
    auto test_3 = (*v1_double) / 2;
    ASSERT_NE(&test_3, nullptr);
    for (size_t i = 0; i < test_2.size(); i++)
    {
        EXPECT_DOUBLE_EQ(test_3[i], (*v1_double)[i] / 2);
        EXPECT_DOUBLE_EQ(test_2[i], test_3[i]);
    }
}

TEST_F(VectorOperationTests, VectorVectorSubtraction)
{
    // verify that 1-2 = -1
    auto test_1 = (*v1_int) - (*v2_int);
    ASSERT_NE(&test_1, nullptr);
    EXPECT_EQ(test_1.size(), v1_int->size());
    EXPECT_EQ(test_1.size(), v2_int->size());
    for (size_t i = 0; i < test_1.size(); i++)
    {
        EXPECT_EQ(-1, test_1[i]);
    }

    // verify that one can subtract a vector from itself
    auto test_2 = (*v1_double) - (*v1_double);
    ASSERT_NE(&test_2, nullptr);
    EXPECT_EQ(test_2.size(), v1_double->size());
    for (size_t i = 0; i < test_2.size(); i++)
    {
        EXPECT_EQ(0, test_2[i]);
    }

    // verify that one cannot add vectors of different sizes
    EXPECT_THROW(auto dim_mismatch_test = (*v1_double) - (*v1_double_big);, std::invalid_argument);
};

TEST_F(VectorOperationTests, VectorNorm)
{
    double test_norm = norm(*v1_complex);
    EXPECT_DOUBLE_EQ(std::sqrt(v1_complex->size()), test_norm);

    Vector<int> t1 = Vector<int>(*v1_int + *v1_int);
    test_norm = norm(t1);
    EXPECT_DOUBLE_EQ(std::sqrt(4 * v1_int->size()), test_norm);
};

TEST_F(VectorOperationTests, VectorDotProduct)
{
    auto testdot = dot(*v1_int, *v1_int);
    EXPECT_EQ(8, testdot);

    auto testdot2 = dot(*v1_int, *v1_double);
    EXPECT_EQ(28, testdot2);

    auto test3 = dot(*v1_int, *v2_int + *v3_int);
    EXPECT_EQ(test3, 40);

    EXPECT_THROW(auto dim_mismatch_test = dot(*v1_int, *v1_double_big);, std::invalid_argument);
}

TEST_F(VectorOperationTests, CompoundVectorExpressions)
{

    // verify associativity of addition and copy to vector from compound tree
    auto test_1a = (*v1_int + *v2_int) + *v3_int;
    auto test_1b = *v1_int + (*v2_int + *v3_int);
    Vector<int> v1a(test_1a);
    Vector<int> v1b(test_1b);
    ASSERT_NE(&test_1a, nullptr);
    for (size_t i = 0; i < v1a.size(); i++)
    {
        EXPECT_EQ(6, test_1a[i]);
        EXPECT_EQ(test_1a[i], test_1b[i]);

        EXPECT_EQ(6, v1a[i]);
        EXPECT_EQ(v1a[i], v1b[i]);
    }


    // verify that type assignment works
    auto test_2 = *v1_double + *v2_double + *v1_int;
    Vector<double> v2(test_2);
    for (size_t i = 0; i < v2.size(); i++)
    {
        EXPECT_EQ(i + 2 * i + 1, v2[i]);
    }

    // verify distributive property for scalar multiplication and division
    auto test_3a = 2 * (*v1_double + *v2_double);
    auto test_3b = 2*(*v1_double) + 2*(*v2_double);
    for (size_t i = 0; i < test_3a.size(); i++)
    {
        EXPECT_DOUBLE_EQ(test_3a[i], test_3b[i]);
    }
}