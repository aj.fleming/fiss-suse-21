add_executable(test_all test_vector.cpp test_eigen.cpp test_matrix.cpp test_sparse.cpp)
target_link_libraries(test_all gtest_main)
target_include_directories(test_all PUBLIC ${PROJECT_SOURCE_DIR}/include/)

gtest_discover_tests(test_all)
