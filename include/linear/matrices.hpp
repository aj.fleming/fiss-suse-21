#ifndef _FISS_MATRICES_H
#define _FISS_MATRICES_H

#include <memory>

#include "fiss_types.hpp"
#include "vectors.hpp"

namespace fiss
{
    using namespace ndarray_traits;

    template <typename M>
    class MatrixAbstract
    {
    public:
        typename ndvector_el_type<M>::type operator[](std::tuple<size_t, size_t> idx) const { return static_cast<const M &>(*this)[idx]; };
        std::tuple<size_t, size_t> shape() const { return static_cast<const M &>(*this).shape(); };
    };

    template <typename E>
    class Matrix : public MatrixAbstract<Matrix<E>>
    {
    private:
        std::tuple<size_t, size_t> m_shape;
        std::unique_ptr<E[]> m_data;

        friend class VectorView<E>;

    public:
        Matrix() : m_shape({0, 0}), m_data(nullptr){};
        Matrix(size_t t_r, size_t t_c) : Matrix<E>(std::tuple<size_t, size_t>({t_r, t_c})){};
        Matrix(std::tuple<size_t, size_t> t_shape) : m_shape{t_shape}, m_data{new E[std::get<0>(t_shape) * std::get<1>(t_shape)]}
        {
            for (size_t i = 0; i < std::get<0>(m_shape) * std::get<1>(m_shape); i++)
            {
                m_data[i] = 0;
            }
        };

        Matrix(const Matrix<E> &m) : m_shape{m.m_shape}, m_data{new E[std::get<0>(m.m_shape) * std::get<1>(m.m_shape)]}
        {
            for (size_t i = 0; i < std::get<0>(m_shape) * std::get<1>(m_shape); i++)
            {
                m_data[i] = m.m_data[i];
            }
        };

        Matrix(Matrix<E> &&m) : m_shape{m.m_shape}, m_data{m.m_data.release()} {};

        std::tuple<size_t, size_t> shape() const
        {
            return m_shape;
        };

        E &operator[](std::tuple<size_t, size_t> idx)
        {
            return m_data[std::get<1>(m_shape) * std::get<0>(idx) + std::get<1>(idx)];
        };

        E operator[](std::tuple<size_t, size_t> idx) const
        {
            return m_data[std::get<1>(m_shape) * std::get<0>(idx) + std::get<1>(idx)];
        };

        template <typename V>
        void set_column(size_t j, VectorAbstract<V> const &c)
        {
            if (c.size() != std::get<0>(m_shape))
            {
                throw std::invalid_argument("matrix column length (number of rows) and vector length not equal");
            }
            for (size_t i = 0; i < std::get<0>(m_shape); i++)
            {
                m_data[std::get<1>(m_shape) * i + j] = c[i];
            }
        }

        VectorView<E> get_col(size_t j)
        {
            return VectorView<E>(m_data.get(), j, std::get<1>(m_shape), std::get<0>(m_shape));
        }

        template <typename V>
        void set_row(size_t i, VectorAbstract<V> const &r)
        {
            if (r.size() != std::get<1>(m_shape))
            {
                throw std::invalid_argument("matrix row length (number of columns) and vector length not equal");
            }
            for (size_t j = 0; j < std::get<1>(m_shape); j++)
            {
                m_data[std::get<1>(m_shape) * i + j] = r[i];
            }
        }
    };
}
#endif // header guard