#ifndef _FISS_VECTOR_TYPETRAITS
#define _FISS_VECTOR_TYPETRAITS

#include <complex>

namespace fiss
{
    typedef std::complex<double> cdouble;

    /**
     * @brief Base class for all vector and vector-like expressions, i.e. accessible with [], has a size
     * Objects of this type represent expression trees, used for lazy evaluation of linear algebra routines.
     *
     * @tparam V The "derived class type", used here for static polymorphism
     */
    template <typename V>
    class VectorAbstract;

    /**
     * @brief Base class for all matrix and matrix-like expressions, i.e. accessible with [{i,j}], has a shape {m,n}
     *
     * @tparam M The "derived class type", used here for static polymorphism
     */
    template <typename M>
    class MatrixAbstract;

    /**
     * @brief A fixed-size vector of numbers.
     *
     * @tparam E The type of elements held in this vector.
     */
    template <typename E>
    class Vector;
    template <typename E>
    class VectorView;

    /**
     * @brief A type of proxy object that represents an operation between two vectors of equal size.
     *
     * @tparam U The type of the first vector argument
     * @tparam V The type of the second vector argument
     */
    template <typename U, typename V>
    class VectorVectorExpr;

    /**
     * @brief A type of proxy object that represents an operation between a scalar and a vector.
     *
     * @tparam U The type of the vector argument.
     * @tparam S The type of the scalar argument.
     */
    template <typename U, typename S>
    class VectorScalarExpr;

    /**
     * @brief A fixed-shape matrix of numbers.
     *
     * @tparam E The numeric type of the matrix elements.
     */
    template <typename E>
    class Matrix;

    /**
     * @brief Holds an immutable view of a matrix.
     *
     * @tparam E The numeric type of the matrix elements;
     */
    template <typename E>
    class MatrixView;

    /**
     * @brief A type of proxy object that represents a matrix-vector product.
     * Takes a matrix of shape {m,n} and a vector of length {n} and yields a vector of length {m}
     *
     * @tparam M The type of the matrix argument
     * @tparam V The type of the vector argument
     */
    template <typename M, typename V>
    class MatrixVectorProduct;

    /**
     * @brief A fixed-shape matrix of numbers stored in CSR format. Has specializations that optimize for symmetry.
     *
     * @tparam E The numeric type of the matrix elements
     */
    template <typename E>
    class SparseMatrix;

    /**
     * @brief A namespace for metafunctions that query array/array element type traits
     *
     */
    namespace ndarray_traits
    {
        /**
         * @brief A metafunction that computes the "result" type of any vector- or matrix-like expression.
         *
         * @tparam V The expression to query.
         */
        template <typename V>
        struct ndvector_el_type
        {
            // hope for the best?
            typedef void type;
        };

        template <typename E>
        struct ndvector_el_type<VectorAbstract<E>>
        {
            typedef typename ndvector_el_type<E>::type type;
        };

        template <typename M>
        struct ndvector_el_type<MatrixAbstract<M>>
        {
            typedef typename ndvector_el_type<M>::type type;
        };

        template <typename E>
        struct ndvector_el_type<VectorView<E>>
        {
            typedef typename ndvector_el_type<Vector<E>>::type type;
        };

        template <typename U, typename V>
        struct ndvector_el_type<VectorVectorExpr<U, V>>
        {
            typedef std::common_type_t<typename ndvector_el_type<U>::type, typename ndvector_el_type<V>::type> type;
        };

        template <typename V, typename S>
        struct ndvector_el_type<VectorScalarExpr<V, S>>
        {
            typedef std::common_type_t<typename ndvector_el_type<V>::type, S> type;
        };

        template <>
        struct ndvector_el_type<Vector<double>>
        {
            typedef double type;
        };

        template <>
        struct ndvector_el_type<Vector<int>>
        {
            typedef int type;
        };

        template <>
        struct ndvector_el_type<Vector<cdouble>>
        {
            typedef cdouble type;
        };

        template <>
        struct ndvector_el_type<Matrix<double>>
        {
            typedef double type;
        };

        template <>
        struct ndvector_el_type<SparseMatrix<double>>
        {
            typedef double type;
        };

        template <typename M, typename V>
        struct ndvector_el_type<MatrixVectorProduct<M, V>>
        {
            typedef std::common_type_t<typename ndvector_el_type<M>::type, typename ndvector_el_type<V>::type> type;
        };
    };
}

#endif