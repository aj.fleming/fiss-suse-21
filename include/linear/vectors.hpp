#ifndef __FISS_VECTORS_H
#define __FISS_VECTORS_H

#include <memory>

#include "fiss_types.hpp"

namespace fiss
{
    using namespace ndarray_traits;

    template <typename V>
    class VectorAbstract
    {
    public:
        typedef typename ndvector_el_type<V>::type el_type;
        /**
         * @brief accesses, or directly computes if necessary, the element at position i in this vector.
         *
         * @param i the element index to access
         * @return el_type An alias for the "result" type of this vector expression.
         */
        el_type operator[](size_t i) const { return static_cast<V const &>(*this)[i]; };

        /**
         * @brief Query the size of this vector
         *
         * @return The size of the vector.
         */
        size_t size() const { return static_cast<V const &>(*this).size(); };
    }; // close VectorAbstract<V>

    template <typename E>
    class Vector : public VectorAbstract<Vector<E>>
    {
    private:
        size_t m_size;
        std::unique_ptr<E[]> m_data;
        friend class VectorView<E>;

    public:
        /**
         * @brief Construct a new Vector object of size 0 and with no associated vector elements
         */
        Vector() : m_size{0}, m_data{nullptr} {};

        /**
         * @brief Construct a new Vector object with some size and intialize all elements to 0.
         *
         * @param t_size The size of the new vector.
         */
        Vector(size_t t_size) : m_size{t_size}, m_data{new E[t_size]}
        {
            for (size_t i = 0; i < m_size; i++)
            {
                m_data[i] = 0;
            }
        };

        /**
         * @brief Construct a new Vector object by copying another vector.
         *
         * @param v The vector to copy
         */
        Vector(const Vector<E> &v) : m_size{v.m_size}, m_data{new E[v.m_size]}
        {
            for (size_t i = 0; i < m_size; i++)
            {
                m_data[i] = v.m_data[i];
            }
        };

        /**
         * @brief Construct a new Vector object by evaluating each element of a provided vector expression.
         *
         * @tparam V The type of vector expression to evaluate
         * @param v The vector expression to evaluate and store in this vector.
         */
        template <typename V>
        Vector(const VectorAbstract<V> &v) : m_size{v.size()}, m_data{new E[v.size()]}
        {
            for (size_t i = 0; i < m_size; i++)
            {
                m_data[i] = v[i];
            }
        }

        /**
         * @brief Construct a new Vector object by stealing resources from another vector.
         *
         * @param v the rvalue vector to steal from.
         */
        Vector(Vector<E> &&v) noexcept : m_size{v.m_size}, m_data{v.m_data.release()} {};

        /**
         * @brief Copy from another vector expression and store that copy in an existing vector.
         */
        template <typename V>
        Vector<E> &operator=(const VectorAbstract<V> &v)
        {
            m_size = v.size();
            m_data.reset(new E[m_size]);
            for (size_t i = 0; i < m_size; i++)
            {
                m_data[i] = v[i];
            }
            return *this;
        };

        /**
         * @brief Copy from a vector and store that copy in an existing vector.
         */
        Vector<E> &operator=(const Vector<E> &v)
        {
            if (this == &v)
            {
                return *this;
            }
            m_size = v.m_size;
            m_data.reset(new E[m_size]);
            for (size_t i = 0; i < m_size; i++)
            {
                m_data[i] = v.m_data[i];
            }
            return *this;
        };

        /**
         * @brief Copy from a temporary object into an existing vector.
         */
        Vector<E> &operator=(Vector<E> &&v) noexcept
        {
            if (this == &v)
            {
                return *this;
            }
            m_size = v.m_size;
            std::swap(m_data, v.m_data);
            return *this;
        };

        /**
         * @brief Get the i-th element of this vector
         *
         * @param i
         * @return E
         */
        E operator[](size_t i) const { return m_data[i]; };
        E &operator[](size_t i) { return m_data[i]; };

        /**
         * @brief Query the size of this vector.
         *
         * @return The size of the vector.
         */
        size_t size() const noexcept { return m_size; };

        /**
         * @brief Get a slice of indices [start, end)
         *
         * @param start
         * @param end
         * @return VectorView<E> Read-only slice of the vector
         */
        VectorView<E> slice(size_t start, size_t end)
        {
            if (start >= end)
            {
                throw std::invalid_argument("starting index of vector slice must be smaller than end");
            }
            return VectorView<E>(m_data.get(), start, end - start, 1);
        }
    }; // end Vector<E>

    /**
     * @brief A class representing access to 1-dimensional strided memory.
     *
     * @tparam E The element data type.
     */
    template <typename E>
    class VectorView : public VectorAbstract<VectorView<E>>
    {
    private:
        // constant pointer to constant values of type E
        // the data array that this vectorview can 'see'
        E const *const m_data;
        const size_t m_size;
        const size_t m_offset;
        const size_t m_stride;

    public:
        /**
         * @brief Construct a new Vector View object that has read-only strided access to the provided block of memory.
         *
         * @param t_data The memory this vector view will read from.
         * @param t_start The index in memory that operator[0] will access
         * @param t_stride
         * @param t_size Upper bound for the indices in this vector view (exclusive)
         */
        VectorView(E *const t_data, size_t t_start, size_t t_stride, size_t t_size) : m_data{t_data},
                                                                                      m_offset{t_start},
                                                                                      m_stride{t_stride},
                                                                                      m_size{t_size} {};

        /**
         * @brief Construct a new Vector View object that can see the entirety of the provided vector.
         *
         * @param t_v
         */
        VectorView(Vector<E> const &t_v) : m_data{t_v.m_data.get()}, m_size{t_v.size()}, m_offset{0}, m_stride{1} {}
        /**
         * @brief Construct a new Vector View object for a provided vector that can see the range [t_start, t_end).
         *
         * @param t_v
         * @param t_start the starting index in the original vector of this vector view (INCLUSIVE)
         * @param t_end the ending index in the original vector of this vector view (EXCLUSIVE)
         */
        VectorView(Vector<E> const &t_v, size_t t_start, size_t t_end) : m_data{t_v.m_data.get()},
                                                                         m_size{t_end - t_start},
                                                                         m_offset{t_start},
                                                                         m_stride{1}
        {
            if (m_size > t_v.size())
            {
                throw std::length_error("cannot construct a vector view larger than its base vector");
            }
            else if (m_offset >= t_v.size())
            {
                throw std::length_error("cannot start a vector view beyond its base vector length");
            }
            else if (t_end > t_v.size())
            {
                throw std::invalid_argument("cannot construct a vector view that overruns its base vector");
            }
        }

        /**
         * @brief Move constructor for temporary vector views.
         *
         * @param t_vv
         */
        VectorView(VectorView<E> const &t_vv) : m_data{t_vv.m_data},
                                                m_size{t_vv.m_size},
                                                m_offset{t_vv.m_offset},
                                                m_stride{t_vv.m_stride} {}

        /**
         * @brief Access the i-th element of this vector view.
         *
         * @param i
         * @return E
         */
        E operator[](size_t i) const
        {
            if (i > m_size)
            {
                throw std::invalid_argument("invalid access to a vector view (i > size)");
            }
            return m_data[i * m_stride + m_offset];
        }

        size_t size() const { return m_size; }
    }; // end VectorView<E>
}

#endif // header guard