#ifndef FISS_LINEAR_H
#define FISS_LINEAR_H

#include <exception>
#include <cmath>
#include <complex>
#include <memory>
#include <string>
#include <tuple>
#include <iostream>

#include "linear/fiss_types.hpp"
#include "linear/vectors.hpp"
#include "linear/matrices.hpp"

namespace fiss
{
    using namespace ndarray_traits;

    /**
     * @brief Computes the dot product of two vectors: s = a_i * b_i
     *
     * @tparam U AbstractVector subtype for argument 1
     * @tparam V AbstractVector subtype for argument 2
     * @param u Vector argument 1
     * @param v Vector Arugment 2
     * @return s The dot product of the two vectors
     */
    template <typename U, typename V>
    std::common_type_t<typename ndvector_el_type<U>::type,
                       typename ndvector_el_type<V>::type>
    dot(const VectorAbstract<U> &u, const VectorAbstract<V> &v)
    {
        if (v.size() != u.size())
        {
            throw std::invalid_argument("cannot dot two vectors of different sizes");
        }
        std::common_type_t<typename ndvector_el_type<U>::type, typename ndvector_el_type<V>::type> s = 0;
        for (size_t i = 0; i < u.size(); i++)
        {
            s += v[i] * u[i];
        }
        return s;
    }

    /**
     * @brief Compute the euclidian 2-norm of a vector.
     *
     * @return double The vector norm.
     */
    template <typename _V>
    double norm(VectorAbstract<_V> const &v)
    {
        double s = 0;
        for (size_t i = 0; i < v.size(); i++)
        {
            s += std::pow(std::abs(v[i]), 2);
        }
        return std::sqrt(s);
    };

    /**
     * @brief Fill a vector with some specified value.
     *
     * @tparam E The numeric type of the vector to fill
     * @param v The vector to fill
     * @param e The value to fill into the vector.
     */
    template <typename E>
    void fillVector(Vector<E> &v, const E e)
    {
        for (size_t i = 0; i < v.size(); i++)
        {
            v[i] = e;
        }
    };

    /**
     * @brief Computes the complex conjugate of a vector expression and returns it as a vector of complex doubles.
     *
     * @tparam V The type of the vector expression to evaluate.
     * @param v The vector expression to evaluate.
     * @return The complex conjugate of the vector expression.
     */
    template <typename V>
    Vector<cdouble> conjVector(const VectorAbstract<V> &v)
    {
        Vector<cdouble> result(v.size());
        for (size_t i = 0; i < v.size(); i++)
        {
            result[i] = std::conj<double>(v[i]);
        }
        return v;
    }

    template <typename _V, typename _S>
    class VectorScalarExpr : public VectorAbstract<VectorScalarExpr<_V, _S>>
    {
    public:
        typedef std::common_type_t<typename ndvector_el_type<_V>::type, _S> el_type;
        typedef el_type (*op_type)(const typename ndvector_el_type<_V>::type, const _S);

    private:
        const _S scalar_arg;
        std::unique_ptr<_V> vector_arg;
        op_type operand;

    public:
        size_t size() const { return vector_arg->size(); };
        el_type operator[](size_t i) const { return operand((*vector_arg)[i], scalar_arg); };

        // construct a copy of a VSE. Deep copies the expression tree
        VectorScalarExpr(VectorScalarExpr<_V, _S> const &t_v) : scalar_arg{t_v.scalar_arg},
                                                                vector_arg{new _V(*t_v.vector_arg)},
                                                                operand{t_v.operand} {};

        // construct a new VSE from a temporary one using move semantics.
        // Used to prevent duplications of temporary vector objects that will be destroyed along with the expression tree they belong to
        VectorScalarExpr(VectorScalarExpr<_V, _S> &&t_v) : scalar_arg{t_v.scalar_arg},
                                                           vector_arg{t_v.vector_arg.release()},
                                                           operand{t_v.operand} {};
        /**
         * @brief Construct a new VectorScalarExpr object from its scalar argument and vector argument.
         *  This constructor is wrapped by @ref operator* and @ref operator/
         * @param t_vector_arg The vector argument (accepts any VectorAbstract<V>)
         * @param t_scalar_arg The scalar argument
         * @param t_op Lambda expression that performs the operation element-wise
         */
        VectorScalarExpr(const _V &t_vector_arg, const _S t_scalar_arg, op_type t_op) : vector_arg{new _V(t_vector_arg)},
                                                                                        scalar_arg{t_scalar_arg},
                                                                                        operand{t_op} {};

        VectorScalarExpr(_V &&t_vector_arg,
                         const _S t_scalar_arg,
                         op_type t_op) : vector_arg{new _V(std::forward<_V>(t_vector_arg))},
                                         scalar_arg{t_scalar_arg},
                                         operand{t_op} {};
    };

    //
    // operator overloads for scalar multiplication and division
    //
    // provides functionality to handle ownership conflicts and scoping issues
    //

    template <typename _V>
    VectorScalarExpr<_V, double> operator*(const VectorAbstract<_V> &v, const double s)
    {
        return VectorScalarExpr<_V, double>(*static_cast<const _V *>(&v), s,
                                            [](const typename ndvector_el_type<_V>::type e1, const double e2)
                                            { return e1 * e2; });
    }

    template <typename _V>
    VectorScalarExpr<_V, double> operator*(const double s, const VectorAbstract<_V> &v)
    {
        return v * s;
    }

    template <typename E>
    VectorScalarExpr<VectorView<E>, double> operator*(Vector<E> const &v, const double s)
    {
        return VectorScalarExpr<VectorView<E>, double>(VectorView<E>(v), s,
                                                       [](const typename ndvector_el_type<VectorView<E>>::type e1, const double e2)
                                                       { return e1 * e2; });
    }

    template <typename E>
    VectorScalarExpr<VectorView<E>, double> operator*(const double s, Vector<E> const &v)
    {
        return v * s;
    }

    template <typename E>
    VectorScalarExpr<Vector<E>, double> operator*(Vector<E> &&v, const double s)
    {
        return VectorScalarExpr<Vector<E>, double>(std::forward<Vector<E>>(v), s,
                                                   [](const typename ndvector_el_type<VectorView<E>>::type e1, const double e2)
                                                   { return e1 * e2; });
    }

    template <typename E>
    VectorScalarExpr<Vector<E>, double> operator*(const double s, Vector<E> &&v)
    {
        return std::forward<Vector<E>>(v) * s;
    }

    template <typename _V>
    VectorScalarExpr<_V, double>
    operator/(const VectorAbstract<_V> &v, const double s)
    {
        return VectorScalarExpr<_V, double>(*static_cast<const _V *>(&v), s,
                                            [](const typename ndvector_el_type<_V>::type e1, const double e2)
                                            { return e1 / e2; });
    }

    template <typename E>
    VectorScalarExpr<VectorView<E>, double>
    operator/(Vector<E> const &v, const double s)
    {
        return VectorScalarExpr<VectorView<E>, double>(VectorView<E>(v), s,
                                                       [](const E e1, const double e2)
                                                       { return e1 / e2; });
    }

    template <typename E>
    VectorScalarExpr<Vector<E>, double>
    operator/(Vector<E> &&v, const double s)
    {
        return VectorScalarExpr<Vector<E>, double>(std::forward<Vector<E>>(v), s,
                                                   [](const E e1, const double e2)
                                                   { return e1 / e2; });
    }

    /**
     * @brief A proxy object that represents a vector-vector expression in the expression tree. Both vector arguments must be the same size.
     * Both template arguments should subclass VectorAbstract.
     *
     * @tparam _U The type of the first vector argument
     * @tparam _V The type of the second vector argument
     */
    template <typename _U, typename _V>
    class VectorVectorExpr : public VectorAbstract<VectorVectorExpr<_U, _V>>
    {
    public:
        typedef std::common_type_t<typename ndvector_el_type<_U>::type, typename ndvector_el_type<_V>::type> el_type;
        typedef el_type (*op_func_type)(const typename ndvector_el_type<_U>::type, const typename ndvector_el_type<_V>::type);

    private:
        std::unique_ptr<_U> arg1;
        std::unique_ptr<_V> arg2;
        op_func_type m_op;

    public:
        size_t size() const { return arg1->size(); };
        el_type operator[](size_t i) const { return m_op((*arg1)[i], (*arg2)[i]); };

        // deep copy constructor for an expression tree
        VectorVectorExpr(VectorVectorExpr<_U, _V> const &t_v) : arg1{new _U(*t_v.arg1)}, arg2{new _V(*t_v.arg2)}, m_op{t_v.m_op} {};
        // copy constructor for an expression tree using move semantics
        VectorVectorExpr(VectorVectorExpr<_U, _V> &&t_v) : arg1{t_v.arg1.release()}, arg2{t_v.arg2.release()}, m_op{t_v.m_op} {};

        VectorVectorExpr(const _U &t_arg1, const _V &t_arg2, op_func_type t_op) : arg1{new _U(t_arg1)}, arg2{new _V(t_arg2)}, m_op{t_op}
        {
            if (arg1->size() != arg2->size())
            {
                throw std::invalid_argument("cannot perform element-wise operations on two vectors of different sizes");
            }
        };

        VectorVectorExpr(_U &&t_arg1, const _V &t_arg2, op_func_type t_op) : arg1{new _U(std::forward<_U>(t_arg1))}, arg2{new _V(t_arg2)}, m_op{t_op}
        {
            if (arg1->size() != arg2->size())
            {
                throw std::invalid_argument("cannot perform element-wise operations on two vectors of different sizes");
            }
        };

        VectorVectorExpr(_U const &t_arg1, _V &&t_arg2, op_func_type t_op) : arg1{new _U(t_arg1)}, arg2{new _V(std::forward<_V>(t_arg2))}, m_op{t_op}
        {
            if (arg1->size() != arg2->size())
            {
                throw std::invalid_argument("cannot perform element-wise operations on two vectors of different sizes");
            }
        };

        VectorVectorExpr(_U &&t_arg1, _V &&t_arg2, op_func_type t_op) : arg1{new _U(t_arg1)}, arg2{new _V(std::forward<_V>(t_arg2))}, m_op{t_op}
        {
            if (arg1->size() != arg2->size())
            {
                throw std::invalid_argument("cannot perform element-wise operations on two vectors of different sizes");
            }
        };
    };

    //
    // vector-vector addition, subtraction, elementwise product, elementwise quotient
    //
    //

    /**
     * @brief Create a VectorVectorExpr object that represents addition between vector-like objects of types _U and _V. v1 and v2 must have the same length.
     *
     * @tparam _U Type of the first vector argument
     * @tparam _V Type of the second vector argument
     * @param v1
     * @param v2
     * @return VectorVectorExpr<_U, _V>
     */
    template <typename _U, typename _V>
    VectorVectorExpr<_U, _V> operator+(const VectorAbstract<_U> &v1, const VectorAbstract<_V> &v2)
    {
        return VectorVectorExpr<_U, _V>(*static_cast<const _U *>(&v1),
                                        *static_cast<const _V *>(&v2),
                                        [](const typename ndvector_el_type<_U>::type e1, const typename ndvector_el_type<_V>::type e2)
                                        { return e1 + e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<VectorView<E>, _V> operator+(Vector<E> const &v1, const VectorAbstract<_V> &v2)
    {
        return VectorVectorExpr<VectorView<E>, _V>(VectorView<E>(v1),
                                                   *static_cast<const _V *>(&v2),
                                                   [](const E e1, const typename ndvector_el_type<_V>::type e2)
                                                   { return e1 + e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<VectorView<E>, _V> operator+(VectorAbstract<_V> const &v1, Vector<E> const &v2)
    {
        return v2 + v1;
    };

    template <typename E, typename F>
    VectorVectorExpr<VectorView<E>, VectorView<F>> operator+(Vector<E> const &v1, Vector<F> const &v2)
    {
        return VectorVectorExpr<VectorView<E>, VectorView<F>>(VectorView<E>(v1),
                                                              VectorView<F>(v2),
                                                              [](const E e1, const F e2)
                                                              { return e1 + e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<Vector<E>, _V> operator+(Vector<E> &&v1, const VectorAbstract<_V> &v2)
    {
        return VectorVectorExpr<Vector<E>, _V>(std::forward<Vector<E>>(v1),
                                               *static_cast<const _V *>(&v2),
                                               [](const E e1, const typename ndvector_el_type<_V>::type e2)
                                               { return e1 + e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<Vector<E>, _V> operator+(VectorAbstract<_V> const &v1, Vector<E> &&v2)
    {
        return std::forward<Vector<E>>(v2) + v1;
    };

    template <typename E, typename F>
    VectorVectorExpr<Vector<E>, Vector<F>> operator+(Vector<E> &&v1, Vector<F> &&v2)
    {
        return VectorVectorExpr<Vector<E>, Vector<F>>(std::forward<Vector<E>>(v1),
                                                      std::forward<Vector<F>>(v2),
                                                      [](const E e1, F e2)
                                                      { return e1 + e2; });
    };

    /**
     * @brief Vector-vector subtraction
     *
     * @tparam _U
     * @tparam _V
     * @param v1
     * @param v2
     * @return VectorVectorExpr<_U, _V>
     */
    template <typename _U, typename _V>
    VectorVectorExpr<_U, _V> operator-(const VectorAbstract<_U> &v1, const VectorAbstract<_V> &v2)
    {
        return VectorVectorExpr<_U, _V>(*static_cast<const _U *>(&v1),
                                        *static_cast<const _V *>(&v2),
                                        [](const typename ndvector_el_type<_U>::type e1, const typename ndvector_el_type<_V>::type e2)
                                        { return e1 - e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<VectorView<E>, _V> operator-(Vector<E> const &v1, const VectorAbstract<_V> &v2)
    {
        return VectorVectorExpr<VectorView<E>, _V>(VectorView<E>(v1),
                                                   *static_cast<const _V *>(&v2),
                                                   [](const E e1, const typename ndvector_el_type<_V>::type e2)
                                                   { return e1 - e2; });
    };

    template <typename _V, typename E>
    VectorVectorExpr<_V, VectorView<E>> operator-(VectorAbstract<_V> const &v1, Vector<E> const &v2)
    {
        return VectorVectorExpr<_V, VectorView<E>>(*static_cast<const _V *>(&v1),
                                                   VectorView<E>(v2),
                                                   [](const typename ndvector_el_type<_V>::type e1, const E e2)
                                                   { return e1 - e2; });
    };

    template <typename E, typename F>
    VectorVectorExpr<VectorView<E>, VectorView<F>> operator-(Vector<E> const &v1, Vector<F> const &v2)
    {
        return VectorVectorExpr<VectorView<E>, VectorView<F>>(VectorView<E>(v1),
                                                              VectorView<F>(v2),
                                                              [](const E e1, const F e2)
                                                              { return e1 - e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<Vector<E>, _V> operator-(Vector<E> &&v1, const VectorAbstract<_V> &v2)
    {
        return VectorVectorExpr<Vector<E>, _V>(std::forward<Vector<E>>(v1),
                                               *static_cast<const _V *>(&v2),
                                               [](const E e1, const typename ndvector_el_type<_V>::type e2)
                                               { return e1 - e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<_V, Vector<E>> operator-(VectorAbstract<_V> const &v1, Vector<E> &&v2)
    {
        return VectorVectorExpr<Vector<E>, _V>(*static_cast<_V const *>(&v1),
                                               std::forward<Vector<E>>(v2),
                                               [](const E e1, const typename ndvector_el_type<_V>::type e2)
                                               { return e1 - e2; });
    };

    template <typename E, typename F>
    VectorVectorExpr<Vector<E>, Vector<F>> operator-(Vector<E> &&v1, Vector<F> &&v2)
    {
        return VectorVectorExpr<Vector<E>, Vector<F>>(std::forward<Vector<E>>(v1),
                                                      std::forward<Vector<F>>(v2),
                                                      [](const E e1, F e2)
                                                      { return e1 - e2; });
    };

    /**
     * @brief Elementwise vector product, i.e. a[i] = b[i]*c[i].
     *
     * @tparam _U Type of the first vector
     * @tparam _V Type of the second vector
     * @param v1
     * @param v2
     * @return VectorVectorExpr<_U, _V>
     */
    template <typename _U, typename _V>
    VectorVectorExpr<_U, _V> operator*(const VectorAbstract<_U> &v1, const VectorAbstract<_V> &v2)
    {
        return VectorVectorExpr<_U, _V>(*static_cast<const _U *>(&v1),
                                        *static_cast<const _V *>(&v2),
                                        [](const typename ndvector_el_type<_U>::type e1, const typename ndvector_el_type<_V>::type e2)
                                        { return e1 * e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<VectorView<E>, _V> operator*(Vector<E> const &v1, const VectorAbstract<_V> &v2)
    {
        return VectorVectorExpr<VectorView<E>, _V>(VectorView<E>(v1),
                                                   *static_cast<const _V *>(&v2),
                                                   [](const E e1, const typename ndvector_el_type<_V>::type e2)
                                                   { return e1 * e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<VectorView<E>, _V> operator*(VectorAbstract<_V> const &v1, Vector<E> const &v2)
    {
        return v2 * v1;
    };

    template <typename E, typename F>
    VectorVectorExpr<VectorView<E>, VectorView<F>> operator*(Vector<E> const &v1, Vector<F> const &v2)
    {
        return VectorVectorExpr<VectorView<E>, VectorView<F>>(VectorView<E>(v1),
                                                              VectorView<F>(v2),
                                                              [](const E e1, const F e2)
                                                              { return e1 * e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<Vector<E>, _V> operator*(Vector<E> &&v1, const VectorAbstract<_V> &v2)
    {
        return VectorVectorExpr<Vector<E>, _V>(std::forward<Vector<E>>(v1),
                                               *static_cast<const _V *>(&v2),
                                               [](const E e1, const typename ndvector_el_type<_V>::type e2)
                                               { return e1 * e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<Vector<E>, _V> operator*(VectorAbstract<_V> const &v1, Vector<E> &&v2)
    {
        return std::forward<Vector<E>>(v2) * v1;
    };

    template <typename E, typename F>
    VectorVectorExpr<Vector<E>, Vector<F>> operator*(Vector<E> &&v1, Vector<F> &&v2)
    {
        return VectorVectorExpr<Vector<E>, Vector<F>>(std::forward<Vector<E>>(v1),
                                                      std::forward<Vector<F>>(v2),
                                                      [](const E e1, F e2)
                                                      { return e1 * e2; });
    };

    /**
     * @brief Element-wise vector quotient, i.e. a[i] = b[i]/c[i]
     *
     * @tparam _U
     * @tparam _V
     * @param v1
     * @param v2
     * @return VectorVectorExpr<_U, _V>
     */
    template <typename _U, typename _V>
    VectorVectorExpr<_U, _V> operator/(const VectorAbstract<_U> &v1, const VectorAbstract<_V> &v2)
    {
        return VectorVectorExpr<_U, _V>(*static_cast<const _U *>(&v1),
                                        *static_cast<const _V *>(&v2),
                                        [](const typename ndvector_el_type<_U>::type e1, const typename ndvector_el_type<_V>::type e2)
                                        { return e1 / e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<VectorView<E>, _V> operator/(Vector<E> const &v1, const VectorAbstract<_V> &v2)
    {
        return VectorVectorExpr<VectorView<E>, _V>(VectorView<E>(v1),
                                                   *static_cast<const _V *>(&v2),
                                                   [](const E e1, const typename ndvector_el_type<_V>::type e2)
                                                   { return e1 / e2; });
    };

    template <typename _V, typename E>
    VectorVectorExpr<_V, VectorView<E>> operator/(VectorAbstract<_V> const &v1, Vector<E> const &v2)
    {
        return VectorVectorExpr<_V, VectorView<E>>(*static_cast<const _V *>(&v1),
                                                   VectorView<E>(v2),
                                                   [](const typename ndvector_el_type<_V>::type e1, const E e2)
                                                   { return e1 / e2; });
    };

    template <typename E, typename F>
    VectorVectorExpr<VectorView<E>, VectorView<F>> operator/(Vector<E> const &v1, Vector<F> const &v2)
    {
        return VectorVectorExpr<VectorView<E>, VectorView<F>>(VectorView<E>(v1),
                                                              VectorView<F>(v2),
                                                              [](const E e1, const F e2)
                                                              { return e1 / e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<Vector<E>, _V> operator/(Vector<E> &&v1, const VectorAbstract<_V> &v2)
    {
        return VectorVectorExpr<Vector<E>, _V>(std::forward<Vector<E>>(v1),
                                               *static_cast<const _V *>(&v2),
                                               [](const E e1, const typename ndvector_el_type<_V>::type e2)
                                               { return e1 / e2; });
    };

    template <typename E, typename _V>
    VectorVectorExpr<_V, Vector<E>> operator/(VectorAbstract<_V> const &v1, Vector<E> &&v2)
    {
        return VectorVectorExpr<Vector<E>, _V>(*static_cast<_V const *>(&v1),
                                               std::forward<Vector<E>>(v2),
                                               [](const E e1, const typename ndvector_el_type<_V>::type e2)
                                               { return e1 / e2; });
    };

    template <typename E, typename F>
    VectorVectorExpr<Vector<E>, Vector<F>> operator/(Vector<E> &&v1, Vector<F> &&v2)
    {
        return VectorVectorExpr<Vector<E>, Vector<F>>(std::forward<Vector<E>>(v1),
                                                      std::forward<Vector<F>>(v2),
                                                      [](const E e1, F e2)
                                                      { return e1 / e2; });
    };

    template <typename M>
    class MatrixAbstract
    {
    public:
        typename ndvector_el_type<M>::type operator[](std::tuple<size_t, size_t> idx) const { return static_cast<const M &>(*this)[idx]; };
        std::tuple<size_t, size_t> shape() const { return static_cast<const M &>(*this).shape(); };
    };

    template <typename E>
    void fillMatrix(Matrix<E> &m, const E e)
    {
        for (size_t i = 0; i < std::get<0>(m.shape()); i++)
        {
            for (size_t j = 0; j < std::get<1>(m.shape()); j++)
            {
                m[{i, j}] = e;
            }
        }
    };

    template <typename E>
    class SparseMatrix : public MatrixAbstract<SparseMatrix<E>>
    {
    private:
        // dimensions of the matrix
        std::tuple<size_t, size_t> m_shape;
        // number of nonzero elements in the matrix
        size_t m_nonzero_elems;
        // index in m_col_idx and m_data where each row starts
        // first element is always zero
        size_t *m_row_idx;
        // column indices of the corresponding entries in m_data
        size_t *m_col_idx;
        // matrix data entries
        E *m_data;

        // is this matrix symmetric?
        bool m_is_symmetric;

    public:
        typedef E el_type;
        SparseMatrix(std::tuple<size_t, size_t> t_shape, size_t t_nonzero,
                     size_t *t_row_idx, size_t *t_col_idx, E *t_data, bool t_sym = false) : m_shape{t_shape}, m_nonzero_elems{t_nonzero},
                                                                                            m_row_idx{t_row_idx}, m_col_idx{t_col_idx},
                                                                                            m_data{t_data}, m_is_symmetric{t_sym} {};

        SparseMatrix(const SparseMatrix<E> &m) : m_shape{m.m_shape}, m_nonzero_elems{m.m_nonzero_elems}, m_is_symmetric{m.m_is_symmetric}
        {
            m_row_idx = new size_t[std::get<0>(m_shape) + 1];
            m_col_idx = new size_t[m_nonzero_elems];
            m_data = new E[m_nonzero_elems];
            for (size_t i = 0; i < std::get<0>(m_shape) + 1; i++)
            {
                m_row_idx[i] = m.m_row_idx[i];
            }
            for (size_t i = 0; i < m_nonzero_elems; i++)
            {
                m_col_idx[i] = m.m_col_idx[i];
                m_data[i] = m.m_data[i];
            }
        };

        SparseMatrix(SparseMatrix<E> &&m) : m_shape{m.m_shape}, m_nonzero_elems{m.m_nonzero_elems},
                                            m_row_idx{m.m_row_idx}, m_col_idx{m.m_col_idx}, m_data{m.m_data},
                                            m_is_symmetric{m.m_is_symmetric}
        {
            m.m_data = nullptr;
            m.m_col_idx = nullptr;
            m.m_row_idx = nullptr;
        };

        ~SparseMatrix()
        {
            delete[] m_row_idx;
            delete[] m_col_idx;
            delete[] m_data;
        };

        std::tuple<size_t, size_t> shape() const { return m_shape; };

        // don't use this if you can use other, better operations
        E operator[](std::tuple<size_t, size_t> idx) const
        {
            // symmetric matrices are stored as lower-triangular matrices
            if (m_is_symmetric && std::get<0>(idx) < std::get<1>(idx))
            {
                idx = {std::get<1>(idx), std::get<0>(idx)};
            }
            for (size_t k = m_row_idx[std::get<0>(idx)]; k < m_row_idx[std::get<0>(idx) + 1]; k++)
            {
                if (std::get<1>(idx) == m_col_idx[k])
                {
                    return m_data[k];
                }
            }
            return 0;
        }

        template <typename V>
        Vector<std::common_type_t<typename V::el_type, el_type>> operator*(const VectorAbstract<V> &arg) const
        {
            if (arg.size() != std::get<1>(m_shape))
            {
                throw std::invalid_argument("dimension mismatch in sparse matrix-vector product");
            }
            Vector<std::common_type_t<typename V::el_type, el_type>> result(arg.size());
            V X = *static_cast<const V *>(&arg);
            // do the symmetry check here rather than in the loop to allow it to be optimized away by the compiler
            if (m_is_symmetric)
            {
                // iterate over stored matrix rows (lower triangular portion)
                for (size_t i = 0; i < std::get<0>(m_shape); i++)
                {
                    for (size_t j = m_row_idx[i]; j < m_row_idx[i + 1]; j++)
                    {
                        // A[i,j] = A[j,i]
                        E Aij = m_data[j];
                        // result[i] += j-th element of row i * result[j]
                        result[i] += Aij * X[m_col_idx[j]];
                        // no need to count diagonal elements twice
                        if (i != m_col_idx[j])
                        {
                            // catch symmetry
                            // result[j] += i-th element of row j * result[i]
                            result[m_col_idx[j]] += X[i] * Aij;
                        }
                    }
                }
            }
            else
            {
                // iterate over matrix rows
                for (size_t i = 0; i < std::get<0>(m_shape); i++)
                {
                    // iterate over matrix columns
                    for (size_t j = m_row_idx[i]; j < m_row_idx[i + 1]; j++)
                    {
                        // only need to multiply non-zero elements
                        result[i] += m_data[j] * X[m_col_idx[j]];
                    }
                }
            }
            return result;
        }
    };

    SparseMatrix<double> loadMatrixMarketFile(const std::string path);

    template <typename E>
    class TridiagMatrix : public MatrixAbstract<TridiagMatrix<E>>
    {
    private:
        std::unique_ptr<E[]> m_sub, m_main, m_sup;
        std::tuple<size_t, size_t> m_shape;

    public:
        template <typename _Sub, typename _Main, typename _Sup>
        TridiagMatrix(VectorAbstract<_Sub> const &t_sub, VectorAbstract<_Main> const &t_main, VectorAbstract<_Sup> const &t_sup) : m_shape{t_main.size(), t_main.size()}
        {
            if (t_sub.size() != t_main.size() - 1 || t_sup.size() != t_main.size() - 1)
            {
                throw std::invalid_argument("dimension mismatch in construction of tridiag matrix from three vectors");
            }
            m_sub.reset(new E[t_sub.size()]);
            m_main.reset(new E[t_main.size()]);
            m_sup.reset(new E[t_main.size()]);
            for (size_t i = 0; i < std::get<0>(m_shape) - 1; i++)
            {
                m_sub[i] = t_sub[i];
                m_main[i] = t_main[i];
                m_sup[i] = t_sup[i];
            }
            m_main[std::get<0>(m_shape) - 1] = t_main[std::get<0>(m_shape) - 1];
        }

        TridiagMatrix(TridiagMatrix<E> const &m) : m_shape{m.m_shape}, m_sub{new E[std::get<0>(m.m_shape) - 1]}, m_main{new E[std::get<0>(m.m_shape)]}, m_sup{new E[std::get<0>(m.m_shape) - 1]}
        {
            for (size_t i = 0; i < std::get<0>(m_shape) - 1; i++)
            {
                m_sub[i] = m.m_sub[i];
                m_main[i] = m.m_main[i];
                m_sup[i] = m.m_sup[i];
            }
            m_main[std::get<0>(m_shape) - 1] = m.m_main[std::get<0>(m_shape) - 1];
        }

        TridiagMatrix(TridiagMatrix<E> &&m) : m_shape{m.m_shape}, m_sub{m.m_sub.release()}, m_main{m.m_main.release()}, m_sup{m.m_sup.release()} {}

        E operator[](std::tuple<size_t, size_t> idx) const
        {
            if (std::get<0>(idx) == std::get<1>(idx))
            {
                // matrix element at [m,m] = main[m]
                return m_main[std::get<0>(idx)];
            }
            else if (std::get<0>(idx) == std::get<1>(idx) + 1)
            {
                // matrix element at [m+1, m] = sub[m]
                return m_sub[std::get<1>(idx)];
            }
            else if (std::get<1>(idx) == std::get<0>(idx) + 1)
            {
                // matrix element at [m, m+1] = sup[m]
                return m_sub[std::get<0>(idx)];
            }
            else
            {
                return 0;
            }
        }

        std::tuple<size_t, size_t> shape() const
        {
            return m_shape;
        }

        template <typename V>
        Vector<std::common_type_t<E, typename ndvector_el_type<V>::type>> operator*(VectorAbstract<V> const &v) const
        {
            if (v.size() != std::get<1>(m_shape))
            {
                throw std::invalid_argument("dimension mismatch in tridiag-vector product");
            }

            Vector<std::common_type_t<E, typename ndvector_el_type<V>::type>> result(std::get<0>(m_shape));
            result[0] = m_main[0] * v[0] + m_sup[0] * v[1];
            for (size_t i = 1; i < std::get<0>(m_shape) - 1; i++)
            {
                result[i] = m_sub[i - 1] * v[i - 1] + m_main[i] * v[i] + m_sup[i] * v[i + 1];
            }
            size_t end = result.size() - 1;
            result[end] = m_sub[end - 1] * v[end - 1] + m_main[end] * v[end];
            return result;
        }
    };

    template <typename _A, typename _X>
    class MatrixVectorProduct : public VectorAbstract<MatrixVectorProduct<_A, _X>>
    {
    public:
        typedef std::common_type_t<typename ndvector_el_type<_A>::type, typename ndvector_el_type<_X>::type> el_type;

    private:
        const _A &A;
        const _X &X;

    public:
        MatrixVectorProduct(const _A &t_A, const _X &t_X) : A{t_A}, X{t_X}
        {
            if (std::get<1>(A.shape()) != X.size())
            {
                throw std::invalid_argument("dimension mismatch in matrix-vector product");
            }
        };
        size_t size() const { return X.size(); };
        el_type operator[](size_t i) const
        {
            el_type s = 0;
            for (size_t j = 0; j < std::get<1>(A.shape()); j++)
            {
                s += A[{i, j}] * X[j];
            }
            return s;
        }
    };

    template <typename _A, typename _X>
    MatrixVectorProduct<_A, _X>
    operator*(MatrixAbstract<_A> const &A, VectorAbstract<_X> const &X)
    {
        return MatrixVectorProduct<_A, _X>(*static_cast<const _A *>(&A), *static_cast<const _X *>(&X));
    };

};

#endif // header guard