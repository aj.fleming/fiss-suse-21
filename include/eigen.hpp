#ifndef FISS_EIGEN_H
#define FISS_EIGEN_H

#include <chrono>
#include <iomanip>
#include <iostream>

#include "linear.hpp"

namespace fiss
{

    namespace eigenval
    {

        template <typename T>
        double findLargestByPowerIteration(const MatrixAbstract<T> &arg, const double TOL, std::ostream &out)
        {
            T A = *static_cast<const T *>(&arg);
            double e, e_old;
            size_t ell = std::get<1>(A.shape());
            Vector<double> q(ell);
            fillVector(q, 1 / std::sqrt(ell));

            int k = 1;
            double diff;
            Vector<double> zk(A * q);
            out << "power=[" << std::endl;
            do
            {
                auto start = std::chrono::steady_clock::now();
                e_old = e;
                q = zk / norm(zk);

                zk = A * q;
                e = dot(q, zk);
                diff = e - e_old;
                auto end = std::chrono::steady_clock::now();
                out << "{'k':" << k << ", 'e':" << std::setprecision(12) << e
                    << ", 'err':" << diff << ", 't':" << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
                    << "}," << std::endl;
                k++;
            } while (std::abs(diff) > TOL && k<100000);
            out << "]" << std::endl;
            return e;
        };

        template <typename M>
        double findLargestByLanczos(const MatrixAbstract<M> &arg, unsigned m, const double TOL, std::ostream &out)
        {
            M A = *static_cast<const M *>(&arg);
            size_t ell = std::get<1>(A.shape());
            Vector<double> v(ell), alpha(m + 1), beta(m + 1);
            fillVector(v, 1 / std::sqrt(ell));

            Matrix<double> krylov_basis(ell, m + 1);
            krylov_basis.set_column(0, v);

            Vector<double> w(A * v);
            alpha[0] = dot(w, v);
            w = w - alpha[0] * v;
            beta[1] = norm(w);
            v = w / beta[1];
            krylov_basis.set_column(1, v);
            out << "lanczos=[" << std::endl;
            for (size_t j = 1; j < m; j++)
            {
                auto start = std::chrono::steady_clock::now();
                w = A * v - beta[j] * krylov_basis.get_col(j - 1);
                alpha[j] = dot(w, v);
                w = w - alpha[j] * v;
                beta[j + 1] = norm(w);
                v = w / beta[j + 1];
                krylov_basis.set_column(j + 1, v);
                auto end = std::chrono::steady_clock::now();
                out << "{'j':" << j
                    << ", 't':" << std::chrono::duration_cast<std::chrono::microseconds>(end - start).count()
                    << "}," << std::endl;
            }
            out << "]" << std::endl;
            VectorView<double> a(alpha, 0, m);
            VectorView<double> b(beta, 1, m);

            return findLargestByPowerIteration(TridiagMatrix<double>(b, a, b), TOL, out);
        }
    };
};
#endif