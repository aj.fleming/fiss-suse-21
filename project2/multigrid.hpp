#ifndef MULTIGRID_H
#define MULTIGRID_H

namespace fiss
{

    class ArrayWrapper2D
    {
    private:
        double *m_data;
    public:
        const int m_rows;
        const int m_cols;

        ArrayWrapper2D(int t_r, int t_c);
        ArrayWrapper2D(const ArrayWrapper2D &m);
        ArrayWrapper2D(ArrayWrapper2D &&m);

        ArrayWrapper2D& operator=(ArrayWrapper2D &&m);

        ~ArrayWrapper2D();

        double &operator()(int i, int j);
        double operator()(int i, int j) const;

        ArrayWrapper2D operator-(const ArrayWrapper2D &m) const;
        ArrayWrapper2D operator*(const double s) const;
    };

    ArrayWrapper2D gaussSiedelSmoother(const ArrayWrapper2D &u, const ArrayWrapper2D &f, double h, int nu);
    ArrayWrapper2D residual(const ArrayWrapper2D &u, const ArrayWrapper2D &f_in, double h);

    ArrayWrapper2D restrict(const ArrayWrapper2D &u);
    ArrayWrapper2D prolong(const ArrayWrapper2D &u);

    ArrayWrapper2D multigrid(int ell, const ArrayWrapper2D &u_in, const ArrayWrapper2D &f_in, double h, int cycle_depth, int nu_1, int nu_2);

    double source_func(double x, double y);
    ArrayWrapper2D source_array(int r, int c, double h);
    ArrayWrapper2D solution_array(int r, int c, double h);

    double maximum_norm(const ArrayWrapper2D &u);
}

#endif