#include <iostream>
#include <fstream>
#define _USE_MATH_DEFINES
#include <cmath>

#include "multigrid.hpp"

using namespace fiss;

int main(int argc, char **argv)
{
    int levels_max = 4;
    int n = (1 << levels_max) + 1;
    double h = 1.0 / (n - 1);

    std::ofstream outfile;
    outfile.open("data/proj2.py");

    ArrayWrapper2D u_m(n, n);
    ArrayWrapper2D u_m_w(n, n);
    const ArrayWrapper2D f_in(source_array(n, n, h));
    ArrayWrapper2D u_ex(solution_array(n, n, h));
    double r_0_inf = maximum_norm(f_in);
    std::cout << "r0 is " << r_0_inf << std::endl;
    outfile << "data=[";
    for (int m = 0; m < 10; m++)
    {
        // v-cycle
        u_m = multigrid(levels_max, u_m, f_in, h, 1, 1, 1);
        ArrayWrapper2D r_m(residual(u_m, f_in, h));
        double res = maximum_norm(r_m) / r_0_inf;
        outfile << "(" << res << ",";
        outfile << maximum_norm(u_m - u_ex) << ",";

        // w-cycle
        u_m_w = multigrid(levels_max, u_m_w, f_in, h, 2, 1, 1);
        ArrayWrapper2D r_m_w(residual(u_m, f_in, h));
        double res_w = maximum_norm(r_m_w) / r_0_inf;
        outfile << res_w << ",";
        outfile << maximum_norm(u_m_w - u_ex);

        outfile << ")," << std::endl;
    }
    outfile << "]" << std::endl;
    outfile.close();
    return 0;
}