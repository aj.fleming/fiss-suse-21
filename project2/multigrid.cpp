#define _USE_MATH_DEFINES
#include <cmath>
#include <stdexcept>
#include <iostream>

#include "multigrid.hpp"

namespace fiss
{
    ArrayWrapper2D::ArrayWrapper2D(int t_r, int t_c) : m_rows{t_r}, m_cols{t_c}
    {
        m_data = new double[t_r * t_c];
        for (int i = 0; i < t_r * t_c; i++)
        {
            m_data[i] = 0;
        }
    }

    ArrayWrapper2D::ArrayWrapper2D(const ArrayWrapper2D &m) : m_rows{m.m_rows}, m_cols{m.m_cols}
    {
        m_data = new double[m_rows * m_cols];
        for (int i = 0; i < m_rows * m_cols; i++)
        {
            m_data[i] = m.m_data[i];
        }
    }

    ArrayWrapper2D::ArrayWrapper2D(ArrayWrapper2D &&m) : m_rows{m.m_rows}, m_cols{m.m_cols}
    {
        m_data = m.m_data;
        m.m_data = nullptr;
    }

    ArrayWrapper2D &ArrayWrapper2D::operator=(ArrayWrapper2D &&m)
    {
        if (m.m_rows != m_rows || m.m_cols != m_cols)
            throw std::invalid_argument("Grids must be same size in assignment");
        if (&m == this)
            return *this;
        delete[] m_data;
        m_data = m.m_data;
        m.m_data = nullptr;
        return *this;
    }

    ArrayWrapper2D::~ArrayWrapper2D()
    {
        delete[] m_data;
    }

    ArrayWrapper2D ArrayWrapper2D::operator-(const ArrayWrapper2D &m) const
    {
        if (m.m_rows != m_rows || m.m_cols != m_cols)
            throw std::invalid_argument("Grids must be same size for subtraction");
        ArrayWrapper2D result(m_rows, m_cols);
        for (int i = 0; i < m_rows; i++)
        {
            for (int j = 0; j < m_cols; j++)
            {
                result(i, j) = (*this)(i, j) - m(i, j);
            }
        }
        return result;
    }

    ArrayWrapper2D ArrayWrapper2D::operator*(const double s) const
    {
        ArrayWrapper2D result(m_rows, m_cols);
        for (int i = 0; i < m_rows; i++)
        {
            for (int j = 0; j < m_cols; j++)
            {
                result(i, j) = (*this)(i, j) * s;
            }
        }
        return result;
    }

    double &ArrayWrapper2D::operator()(int r, int c)
    {
        return m_data[r * m_cols + c];
    }

    double ArrayWrapper2D::operator()(int r, int c) const
    {
        return m_data[r * m_cols + c];
    }

    double source_func(double x, double y)
    {
        return 8 * M_PI * M_PI * std::sin(2 * M_PI * x) * std::sin(2 * M_PI * y);
    }

    // modifies solution u in place
    // see lecture 11 slide 7
    ArrayWrapper2D gaussSiedelSmoother(const ArrayWrapper2D &u_old, const ArrayWrapper2D &f, double h, int nu)
    {
        ArrayWrapper2D u(u_old);
        for (int m = 0; m < nu; m++)
        {
            // points at the edge are set to 0 by Dirichlet BC
            for (int i = 1; i < u.m_rows - 1; i++)
            {
                for (int j = 1; j < u.m_cols - 1; j++)
                {
                    u(i, j) = 0.25 * (h * h * f(i, j) + u(i - 1, j) + u(i + 1, j) + u(i, j - 1) + u(i, j + 1));
                }
            }
        }
        return u;
    }

    ArrayWrapper2D residual(const ArrayWrapper2D &u, const ArrayWrapper2D &f_in, double h)
    {
        double h_sqr = h * h;
        ArrayWrapper2D r(u.m_rows, u.m_cols);
        for (int i = 1; i < r.m_rows - 1; i++)
        {
            for (int j = 1; j < r.m_cols; j++)
            {
                r(i, j) = f_in(i, j) + (u(i - 1, j) + u(i, j - 1) - 4 * u(i, j) + u(i + 1, j) + u(i, j + 1)) / h_sqr;
            }
        }
        return r;
    }

    ArrayWrapper2D restrict(const ArrayWrapper2D &u)
    {
        int n_coarse = (u.m_cols - 1) / 2 + 1;
        ArrayWrapper2D u_coarse(n_coarse, n_coarse);
        int ii, jj;
        for (int i = 1; i < u_coarse.m_rows - 1; i++)
        {
            ii = 2 * i;
            for (int j = 1; j < u_coarse.m_cols - 1; j++)
            {
                jj = 2 * j;
                // stencil from exercise 11
                u_coarse(i, j) = (u(ii - 1, jj - 1) + 2 * u(ii, jj - 1) + u(ii + 1, jj - 1) + 2 * u(ii - 1, jj) + 4 * u(ii, jj) + 2 * u(ii + 1, jj) + u(ii - 1, jj + 1) + 2 * u(ii, jj + 1) + u(ii + 1, jj + 1)) / 16;
            }
        }
        return u_coarse;
    }

    ArrayWrapper2D prolong(const ArrayWrapper2D &u)
    {
        int n_fine = (u.m_cols - 1) * 2 + 1;
        ArrayWrapper2D u_fine(n_fine, n_fine);
        int ii, jj;
        for (int i = 1; i < u.m_rows - 1; i++)
        {
            ii = 2 * i;
            // this memory access pattern is SHIT
            for (int j = 1; j < u.m_cols - 1; j++)
            {
                jj = 2 * j;
                u_fine(ii - 1, jj - 1) += u(i, j) / 4;
                u_fine(ii, jj - 1) += u(i, j) / 2;
                u_fine(ii + 1, jj - 1) += u(i, j) / 4;
                u_fine(ii - 1, jj) += u(i, j) / 2;
                u_fine(ii, jj) += u(i, j);
                u_fine(ii + 1, jj) += u(i, j) / 2;
                u_fine(ii - 1, jj + 1) += u(i, j) / 4;
                u_fine(ii, jj + 1) += u(i, j) / 2;
                u_fine(ii + 1, jj + 1) += u(i, j) / 4;
            }
        }
        // you fine :eyes:
        return u_fine;
    }

    ArrayWrapper2D source_array(int r, int c, double h)
    {
        ArrayWrapper2D f(r, c);
        for (int i = 1; i < r - 1; i++)
        {
            for (int j = 1; j < r - 1; j++)
            {
                f(i, j) = source_func(i * h, j * h);
            }
        }
        return f;
    }

    ArrayWrapper2D solution_array(int r, int c, double h)
    {
        ArrayWrapper2D f(r, c);
        for (int i = 1; i < r - 1; i++)
        {
            for (int j = 1; j < r - 1; j++)
            {
                f(i, j) = std::sin(2*M_PI*i*h)*std::sin(2*M_PI*j*h);
            }
        }
        return f;
    }

    double maximum_norm(const ArrayWrapper2D &u)
    {
        double r = std::abs(u(0, 0));
        for (int i = 1; i < u.m_rows; i++)
        {
            for (int j = 0; j < u.m_cols; j++)
            {
                if (r < std::abs(u(i, j)))
                {
                    r = std::abs(u(i, j));
                }
            }
        }
        return r;
    }

    ArrayWrapper2D multigrid(int ell, const ArrayWrapper2D &u_in, const ArrayWrapper2D &f_in, double h, int cycle_depth, int nu_1, int nu_2)
    {
        ArrayWrapper2D u_L(gaussSiedelSmoother(u_in, f_in, h, nu_1));
        ArrayWrapper2D r_L(residual(u_L, f_in, h));
        ArrayWrapper2D r_coarser(restrict(r_L));
        ArrayWrapper2D e_coarser(r_coarser.m_rows, r_coarser.m_cols);
        // base case check
        if (ell == 1)
        {
            e_coarser = gaussSiedelSmoother(e_coarser, r_coarser * -1, 2 * h, 1);
        }
        else
        {

            for (int iter = 0; iter < cycle_depth; iter++)
            {
                ArrayWrapper2D f_next(r_coarser * -1);
                e_coarser = multigrid(ell - 1, e_coarser, f_next, 2 * h, cycle_depth, nu_1, nu_2);
            }
        }
        ArrayWrapper2D e_L(prolong(e_coarser));
        return gaussSiedelSmoother(u_L - e_L, f_in, h, nu_2);
    }
}