
#include <chrono>
#include <fstream>
#include <iostream>
#include <iomanip>
#include <string>

#include "project3.hpp"

#include "linear.hpp"
#include "eigen.hpp"

using namespace fiss;

int main(int argc, char **argv)
{
    std::string matrix_path(std::string(MATRIX_FILE_PATH) + std::string("nos6.mtx"));
    SparseMatrix<double> testSparse(loadMatrixMarketFile(matrix_path));
    std::string s3rmt(std::string(MATRIX_FILE_PATH) + std::string("s3rmt3m3.mtx"));
    SparseMatrix<double> s3(loadMatrixMarketFile(s3rmt));

    std::ofstream datafile;
    datafile.open(std::string(DATA_FILE_OUTPUT_PATH) + std::string("power_nos6.py"));
    datafile << std::scientific;
    auto start = std::chrono::steady_clock::now();
    double lambda_1 = eigenval::findLargestByPowerIteration(testSparse, 1.0e-8, datafile);
    auto end = std::chrono::steady_clock::now();
    std::cout << "found largest eigenvalue after " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms" << std::endl;
    std::cout << "largest eigenval of nos6 is " << lambda_1 << std::endl
              << std::endl;
    datafile.close();

    datafile.open(std::string(DATA_FILE_OUTPUT_PATH)+std::string("power_s3r.py"));
    datafile << std::scientific;
    start = std::chrono::steady_clock::now();
    double lambda_1_s3rmt = eigenval::findLargestByPowerIteration(s3, 1.0e-8, datafile);
    end = std::chrono::steady_clock::now();
    std::cout << "found largest eigenvalue after " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms" << std::endl;
    std::cout << "largest eigenval of s3rmt by power iteration is " << lambda_1_s3rmt << std::endl
              << std::endl;
    datafile.close();

    size_t krylov_sizes[] = {30, 50, 75, 100, 200};
    double tolerances[] = {1.0e-14, 1.0e-14, 1.0e-14, 1.0e-16, 1.0e-16};

    double actual_eval = 9.5986080894852857e+03;

    std::cout << std::scientific;

    for (int i = 0; i < 5; i++)
    {
        datafile.open(std::string(DATA_FILE_OUTPUT_PATH)+std::string("lanczos_")+std::to_string(krylov_sizes[i])+std::string(".py"));
        datafile << std::scientific;
        start = std::chrono::steady_clock::now();
        double lanczos_lambda = eigenval::findLargestByLanczos(s3, krylov_sizes[i], tolerances[i], datafile);
        end = std::chrono::steady_clock::now();
        std::cout << "found largest eigenvalue after " << std::chrono::duration_cast<std::chrono::milliseconds>(end - start).count() << "ms" << std::endl;
        std::cout << "largest eigenvalue of s3rmt by Lanczos with m=" << krylov_sizes[i] << " is " << std::setprecision(8) << lanczos_lambda << std::endl
                  << " relative error is " << std::setprecision(2) << std::abs(lanczos_lambda - actual_eval) * 100 / actual_eval << "%" << std::endl
                  << std::endl;
        datafile.close();
    }

    return 0;
}