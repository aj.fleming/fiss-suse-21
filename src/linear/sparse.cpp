#include <stdio.h>

#include <algorithm>
#include <cmath>
#include <stdexcept>
#include <string>
#include <vector>

#include "mmio.h"
#include "linear.hpp"

namespace fiss
{
    struct coo_element
    {
        // row index
        int i;
        // column index
        int j;
        // data item
        double data;
    };

    bool cmp_entries_by_row(coo_element &e1, coo_element &e2)
    {
        if (e1.i < e2.i)
        {
            return true;
        }
        else if (e1.i == e2.i)
        {
            return e1.j < e2.j;
        }
        return false;
    }

    SparseMatrix<double> loadMatrixMarketFile(const std::string path)
    {
        FILE *in = fopen(path.c_str(), "r");
        MM_typecode matrixType;
        int rows, cols, nnz;
        mm_read_header(in, &matrixType, &rows, &cols, &nnz);
        bool is_sym = mm_is_symmetric(matrixType);
        std::vector<coo_element> entries;
        for (int i = 0; i < nnz; i++)
        {
            entries.push_back(coo_element());
            fscanf(in, "%d %d %lg\n", &entries[i].i, &entries[i].j, &entries[i].data);
        }
        fclose(in);

        std::sort(entries.begin(), entries.end(), cmp_entries_by_row);
        // compress the data to CSR from COO
        size_t *rows_idx = new size_t[rows + 1];
        size_t *col_idx = new size_t[nnz];
        double *data = new double[nnz];
        rows_idx[0] = 0;
        int cur_row = 1;
        int row_elem_count = 0;
        for (int elem = 0; elem < nnz; elem++)
        {
            if (entries[elem].i > cur_row)
            {
                rows_idx[cur_row] = elem;
                cur_row++;
                row_elem_count = 0;
            }
            col_idx[elem] = entries[elem].j - 1;
            data[elem] = entries[elem].data;
            row_elem_count++;
        }
        rows_idx[rows] = nnz;
        return SparseMatrix<double>({rows, cols}, nnz, rows_idx, col_idx, data, is_sym);
    }

}