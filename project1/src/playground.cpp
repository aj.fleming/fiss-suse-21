#include <iostream>
#include <fstream>
#include <cmath>
#include <stdio.h>
#include "vector.hpp"
#include "matrix.hpp"
#include "fiss.hpp"

using namespace std;
using namespace fiss;
class Test
{
private:
    int *data;

public:
    const unsigned size;
    Test(unsigned t_size) : size{t_size}
    {
        data = new int[size];
        for (unsigned i = 0; i < size; i++)
        {
            data[i] = i;
        }
    }

    ~Test()
    {
        delete[] data;
    }

    int &operator[](unsigned i)
    {
        return data[i];
    }
};

void fiss::pretty_print_matrix(MatrixLike &m)
{
    std::cout << "[" << m.rows() << " x " << m.cols() << "]" << std::endl;
    std::cout << "[";
    for (int i = 0; i < m.rows(); i++)
    {
        if (i > 0)
            std::cout << " ";
        for (int j = 0; j < m.cols(); j++)
        {
            printf("%+6.3f ", m(i, j));
        }
        if (i < m.rows() - 1)
            std::cout << std::endl;
    }
    std::cout << "]" << std::endl;
}

void fiss::pretty_print_vector(VectorLike &v)
{
    std::cout << "[";
    for (int i = 0; i < v.length(); i++)
    {
        std::cout << " " << v[i];
    }
    std::cout << " ]" << std::endl;
}

int main(int arc, char **argv)
{
    std::string p = "matrices/s3rmt3m3.mtx";
    SparseCSR *sm = load_from_mm_file(p.c_str());

    std::ofstream csrfile;
    csrfile.open("s3rmt3m3_as_csr.py");
    csrfile << "rows= " << sm->rows() << std::endl;
    csrfile << "cols= " << sm->cols() << std::endl;
    csrfile << "n_nz= " << sm->nonzero_elements() << std::endl;
    csrfile << "row_start_indices = [";
    for (int i = 0; i < sm->rows() + 1; i++)
    {
        csrfile << sm->m_row_start_idx[i];
        if (i != sm->rows()) csrfile << ",";
    }
    csrfile << "]"<<std::endl << "column_indices = [";
    for (int i = 0; i < sm->nonzero_elements(); i++)
    {
        csrfile << sm->m_col_idx[i];
        if (i != sm->nonzero_elements()-1) csrfile << ",";
    }
    csrfile << "]" << std::endl << "data_elements = [";
    for (int i = 0; i < sm->nonzero_elements(); i++)
    {
        csrfile << sm->m_data[i] << " ";
        if (i != sm->nonzero_elements()-1) csrfile << ",";
    }
    csrfile << "]"<<std::endl;
    csrfile.close();
    delete sm;
}