#include <stdio.h>
#include <iostream>
#include <string>
#include <chrono>

#include "fiss.hpp"
#include "vector.hpp"
#include "matrix.hpp"

using namespace fiss;

void fiss::pretty_print_matrix(MatrixLike &m)
{
    std::cout << "[" << m.rows() << " x " << m.cols() << "]" << std::endl;
    std::cout << "[";
    for (int i = 0; i < m.rows(); i++)
    {
        if (i > 0)
            std::cout << " ";
        for (int j = 0; j < m.cols(); j++)
        {
            printf("%+6.3e ", m(i, j));
        }
        if (i < m.rows() - 1)
            std::cout << std::endl;
    }
    std::cout << "]" << std::endl;
}

void fiss::pretty_print_vector(VectorLike &v)
{
    std::cout << "[";
    for (int i = 0; i < v.length(); i++)
    {
        if (i > 0)
            std::cout << " ";
        printf("%+6.3e", v[i]);
    }
    std::cout << "]" << std::endl;
}

int main(int argc, char **argv)
{
    SparseCSR *orsirr = load_from_mm_file("matrices/orsirr_1.mtx");
    SparseCSR *s3 = load_from_mm_file("matrices/s3rmt3m3.mtx");
    Vector orsirr_sol(orsirr->rows());
    Vector s3_sol(s3->rows());
    for (int i = 0; i < orsirr_sol.length(); i++)
    {
        orsirr_sol[i] = 1;
    }
    for (int i = 0; i < s3_sol.length(); i++)
    {
        s3_sol[i] = 1;
    }

    Vector b_orsirr(orsirr->dot(orsirr_sol));
    Vector b_s3(s3->dot(s3_sol));
    Vector x0_orsirr(b_orsirr.length());
    Vector x0_s3(b_s3.length());

    std::string prefix = "data/restarted_gmres_";
    std::string middle = "_precond_";
    std::string suffix = ".py";
    int krylov_vector_counts[] = {25, 50, 75, 100};
    int j = 0;
    for (int i = 0; i < 4; i++)
    {
        std::string outfile = prefix + std::to_string(krylov_vector_counts[i]) + middle + std::to_string(j) + suffix;
        auto start = std::chrono::high_resolution_clock::now();
        Vector gmres_result = restarted_gmres(orsirr, b_orsirr, x0_orsirr, b_orsirr.length(),
                                              krylov_vector_counts[i], 1.0e-8, outfile, j);
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = end - start;
        std::cout << "GMRES completed in " << elapsed.count() << std::endl;
    }
    j = 1;
    for (int i = 0; i < 4; i++)
    {
        std::string outfile = prefix + std::to_string(krylov_vector_counts[i]) + middle + std::to_string(j) + suffix;
        auto start = std::chrono::high_resolution_clock::now();
        Vector gmres_result = restarted_gmres_jacobi(orsirr, b_orsirr, x0_orsirr, b_orsirr.length(),
                                                     krylov_vector_counts[i], 1.0e-8, outfile, j);
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = end - start;
        std::cout << "GMRES completed in " << elapsed.count() << std::endl;
    }
    j = 2;
    for (int i = 0; i < 4; i++)
    {
        std::string outfile = prefix + std::to_string(krylov_vector_counts[i]) + middle + std::to_string(j) + suffix;
        auto start = std::chrono::high_resolution_clock::now();
        Vector gmres_result = restarted_gmres_gauss(orsirr, b_orsirr, x0_orsirr, b_orsirr.length(),
                                                    krylov_vector_counts[i], 1.0e-8, outfile, j);
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = end - start;
        std::cout << "GMRES completed in " << elapsed.count() << std::endl;
    }
    auto start = std::chrono::high_resolution_clock::now();
    Vector fullg_result(full_gmres(orsirr, b_orsirr, x0_orsirr, 1.0e-8, "data/fullgmres.py"));
    auto end = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = end - start;
    std::cout << "Full gmres completed in " << elapsed.count() << std::endl;

    start = std::chrono::high_resolution_clock::now();
    Vector fullg_j_result(full_gmres_jacobi(orsirr, b_orsirr, x0_orsirr, 1.0e-8, "data/fullgmres_j.py"));
    end = std::chrono::high_resolution_clock::now();
    elapsed = end - start;
    std::cout << "Full gmres completed in " << elapsed.count() << std::endl;

    start = std::chrono::high_resolution_clock::now();
    Vector fullg_gs_result(full_gmres_gauss(orsirr, b_orsirr, x0_orsirr, 1.0e-8, "data/fullgmres_gs.py"));
    end = std::chrono::high_resolution_clock::now();
    elapsed = end - start;
    std::cout << "Full gmres completed in " << elapsed.count() << std::endl;

    start = std::chrono::high_resolution_clock::now();
    Vector cg_result(conjugate_gradient(s3, b_s3, x0_s3, s3_sol, 1.0e-8, "data/cg.py"));
    end = std::chrono::high_resolution_clock::now();
    elapsed = end - start;
    std::cout << "Conjugate gradient completed in " << elapsed.count() << std::endl;
    std::cout << "Conjugate gradient Ax-b comparison: ";
    double r = (b_s3 - s3->dot(cg_result)).norm();
    std::cout << r << std::endl;

    delete orsirr;
    delete s3;
}