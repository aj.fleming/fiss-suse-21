#ifndef FISS_VECTOR_H
#define FISS_VECTOR_H

namespace fiss
{
    class Vector;
    class VectorView;

    class VectorLike
    {
    private:
        int m_length;

    protected:
        void setLength(int t_length);

    public:
        VectorLike(int t_length);
        // Access element i of this vector. 0-indexed. Returns a reference to element i if modification of the value is possible.
        virtual double &operator[](int i) = 0;
        virtual double operator[](int i) const = 0;

        // Compute and return the norm of this vector-like
        double norm() const;
        
        // Length of the vector
        int length() const;        
        // scalar product
        Vector operator*(const double s) const;
        Vector operator*(const Vector &v) const;
        // vector dot product
        double dot(const VectorLike &v) const;
        // vector addition/subtraction
        Vector operator+(const VectorLike &v) const;
        Vector operator-(const VectorLike &v) const;

        virtual VectorView slice(int start, int end) = 0;
    };
    // A Vector. Owns its data.
    class Vector : public VectorLike
    {
    private:
        double *m_data;

    public:
        Vector();
        Vector(int t_size);
        Vector(int t_size, double *t_data); 
        ~Vector();

        Vector(const Vector &v);
        Vector(Vector &&v);
        Vector(const VectorLike &vl);

        Vector &operator=(const Vector &v);
        Vector &operator=(Vector &&v);

        // makes sense to define -= and += to modify the vector in-place
        void operator-=(const VectorLike &v);
        void operator+=(const VectorLike &v);

        double &operator[](int i);
        double operator[](int i) const;
        VectorView slice(int start, int end);

    };

    // A vector. Instances of this class DO NOT own their data.
    class VectorView : public VectorLike
    {
    private:
        int m_stride;
        double *m_data_start;

    public:
        VectorView(int t_length, int t_stride, double *t_data_start);
        VectorView();
        VectorView(VectorView &vv);
        VectorView(VectorView &&vv);

        VectorView &operator=(const VectorView &vv);
        VectorView &operator=(VectorView &&vv);

        double &operator[](int i);
        double operator[](int i) const;
        VectorView slice(int start, int end);
    };

    // scalar-vector product; returns a Vector object that has a new copy of the data.
    Vector scale_vector(const double s, const VectorLike &v);
    // vector addition
    Vector v_add(const VectorLike &v1, const VectorLike &v2);
    // vector subraction
    Vector v_diff(const VectorLike &v1, const VectorLike &v2);

};
#endif