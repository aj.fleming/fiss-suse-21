#include <stdio.h>
#include <vector>
#include <string>
#include <algorithm>

#include "fiss.hpp"
#include "sparse.hpp"
#include "mmio/mmio.h"

namespace fiss
{

    /*
    struct and comparison routine to allow sorting of data into row-first or column-first
    */
    struct entry
    {
        unsigned row;
        unsigned col;
        double data;
    };

    bool cmp_entries_by_row(entry &e1, entry &e2)
    {
        if (e1.row < e2.row)
        {
            return true;
        }
        else if (e1.row == e2.row)
        {
            return e1.col < e2.col;
        }
        return false;
    }

    bool cmp_entries_by_col(entry &e1, entry &e2)
    {
        if (e1.col < e1.col)
        {
            return true;
        }
        else if (e1.col == e2.col)
        {
            return e1.row < e2.row;
        }
        return false;
    }

    SparseCSR *load_from_mm_file(const char *path)
    {
        FILE *matfile = fopen(path, "r");
        MM_typecode mat_type;
        int r, c, nz;
        mm_read_header(matfile, &mat_type, &r, &c, &nz);
        std::vector<entry> entries;
        for (int i = 0; i < nz; i++)
        {
            entries.push_back(entry());
            fscanf(matfile, "%d %d %lg\n", &entries[i].row, &entries[i].col, &entries[i].data);
        }
        std::sort(entries.begin(), entries.end(), cmp_entries_by_row);

        // compress the data to CSR from COO
        unsigned *rows_idx = new unsigned[r + 1];
        unsigned *col_idx = new unsigned[nz];
        double *data = new double[nz];
        rows_idx[0] = 0;
        int cur_row = 1;
        int row_elem_count = 0;
        for (int elem = 0; elem < nz; elem++)
        {
            if (entries[elem].row > cur_row)
            {
                rows_idx[cur_row] = elem;
                cur_row++;
                row_elem_count = 0;
            }
            col_idx[elem] = entries[elem].col - 1;
            data[elem] = entries[elem].data;
            row_elem_count++;
        }
        rows_idx[r] = nz;

        if (mm_is_symmetric(mat_type))
        {
            return new SparseCSRSymmetric(r, nz, rows_idx, col_idx, data);
        }
        else
        {
            return new SparseCSR(r, c, nz, rows_idx, col_idx, data);
        }
    }
}