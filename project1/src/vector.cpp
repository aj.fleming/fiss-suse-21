#include <cmath>
#include <stdexcept>
#include "vector.hpp"

namespace fiss
{

    // VectorLike abstract definitions:
    VectorLike::VectorLike(int t_length) : m_length{t_length}
    {
    }

    void VectorLike::setLength(int t_length)
    {
        m_length = t_length;
    }

    int VectorLike::length() const
    {
        return m_length;
    }

    // 2-norm
    double VectorLike::norm() const
    {
        double n = 0;
        for (int i = 0; i < m_length; i++)
        {
            n = n + (*this)[i] * (*this)[i];
        }
        return std::sqrt(n);
    }

    Vector VectorLike::operator*(const double s) const
    {
        double *new_data = new double[m_length];
        for (int i = 0; i < m_length; i++)
        {
            new_data[i] = s * (*this)[i];
        }
        return Vector(m_length, new_data);
    }

    Vector VectorLike::operator*(const Vector &v) const
    {
        Vector result(m_length);
        for(int i = 0; i<m_length; i++){
            result[i] = (*this)[i] * v[i];
        }
        return result;
    }
    

    //  In general, the semantics for vector-vector operations are the same; let the subclasses handle cleaning up their resources
    //  and determining memory access patterns
    double VectorLike::dot(const VectorLike &v) const
    {
        if(v.m_length != m_length) throw std::invalid_argument("Cannot dot vectors of different sizes");

        double s = 0;
        for (int i = 0; i < m_length; i++)
        {
            s = s + (*this)[i] * v[i];
        }
        return s;
    }

    Vector VectorLike::operator+(const VectorLike &v) const
    {
        if(v.length() != m_length) throw std::invalid_argument("Cannot add vectors of different sizes");
        double *new_data = new double[m_length];
        for (int i = 0; i < m_length; i++)
        {
            new_data[i] = (*this)[i] + v[i];
        }
        return Vector(m_length, new_data);
    }

    Vector VectorLike::operator-(const VectorLike &v) const
    {
        if(v.length() != m_length) throw std::invalid_argument("Cannot subtract vectors of different sizes");
        double *new_data = new double[m_length];
        for (int i = 0; i < m_length; i++)
        {
            new_data[i] = (*this)[i] - v[i];
        }
        return Vector(m_length, new_data);
    }

    // 'proper' vectors
    // these own their data and have both copy and move semantics when necessary.
    Vector::Vector() : VectorLike{0}, m_data{nullptr}
    {
    }

    Vector::Vector(int t_size) : VectorLike{t_size}
    {
        m_data = new double[length()];
        for (int i = 0; i < length(); i++){
            m_data[i] = 0;
        }
    }

    //Construct a vector with known size and data
    Vector::Vector(int t_length, double *t_data) : VectorLike{t_length}, m_data{t_data}
    {
    }

    // Deep copy constructor from another vector
    Vector::Vector(const Vector &v) : VectorLike{v.length()}
    {
        m_data = new double[length()];
        for (int i = 0; i < length(); i++)
        {
            m_data[i] = v.m_data[i];
        }
    }

    // Shallow copy / move constructor
    Vector::Vector(Vector &&v) : VectorLike{v.length()}, m_data{v.m_data}
    {
        v.m_data = nullptr;
    }

    // copy constructor from a VectorView
    Vector::Vector(const VectorLike &vl) : VectorLike{vl.length()}
    {
        m_data = new double[length()];
        for(int i =0; i < length(); i++){
            m_data[i] = vl[i];
        }
    }

    Vector::~Vector()
    {
        delete[] m_data;
    }

    // deep copy assignment
    Vector &Vector::operator=(const Vector &v)
    {
        if (&v == this)
        {
            return *this;
        }
        delete[] m_data;
        setLength(v.length());
        m_data = new double[length()];
        for (int i = 0; i < length(); i++)
        {
            m_data[i] = v.m_data[i];
        }

        return *this;
    }

    // move assignment
    Vector &Vector::operator=(Vector &&v)
    {
        if (&v == this)
        {
            return *this;
        }
        delete[] m_data;
        setLength(v.length());
        m_data = v.m_data;
        v.m_data = nullptr;

        return *this;
    }

    void Vector::operator-=(const VectorLike &v){
        if(v.length() != length()) throw std::invalid_argument("vector size mismatch in -=");
        for(int i = 0; i<length(); i++){
            m_data[i] = m_data[i] - v[i];
        }
    }

    void Vector::operator+=(const VectorLike &v){
        if(v.length() != length()) throw std::invalid_argument("vector size mismatch in +=");
        for(int i = 0; i<length(); i++){
            m_data[i] = m_data[i] + v[i];
        }
    }

    double &Vector::operator[](int i)
    {
        return m_data[i];
    }

    double Vector::operator[](int i) const
    {
        return m_data[i];
    }

    // return a slice of this vector. end index is EXCLUSIVE
    VectorView Vector::slice(int start, int end){
        return VectorView(end-start, 1, &m_data[start]);
    }

    // VectorView methods
    VectorView::VectorView() : VectorLike{0}, m_stride{0}, m_data_start{nullptr}
    {
    }

    VectorView::VectorView(int t_length, int t_stride, double *t_data) : VectorLike{t_length}, m_stride{t_stride}, m_data_start{t_data}
    {
    }

    VectorView::VectorView(VectorView &vv) : VectorLike{vv.length()}, m_stride{vv.m_stride}, m_data_start{vv.m_data_start}
    {
    }

    VectorView::VectorView(VectorView &&vv) : VectorLike{vv.length()}, m_stride{vv.m_stride}, m_data_start{vv.m_data_start}
    {
    }

    VectorView &VectorView::operator=(const VectorView &vv)
    {
        setLength(vv.length());
        m_stride = vv.m_stride;
        m_data_start = vv.m_data_start;

        return *this;
    }

    VectorView &VectorView::operator=(VectorView &&vv)
    {
        setLength(vv.length());
        m_stride = vv.m_stride;
        m_data_start = vv.m_data_start;
        return *this;
    }

    double VectorView::operator[](int i) const
    {
        return m_data_start[m_stride * i];
    }

    double &VectorView::operator[](int i)
    {
        return m_data_start[m_stride * i];
    }

    VectorView VectorView::slice(int start, int end){
        return VectorView(end-start, m_stride, &m_data_start[start]);
    }

    // Scalar product
    Vector scale_vector(const double s, const VectorLike &v)
    {
        double *new_data = new double[v.length()];
        for (int i = 0; i < v.length(); i++)
        {
            new_data[i] = s * v[i];
        }
        return Vector(v.length(), new_data);
    }

    Vector v_add(const VectorLike &v1, const VectorLike &v2)
    {
        if(v1.length() != v2.length()) throw std::invalid_argument("Cannot add vectors of different sizes");
        double *new_data = new double[v1.length()];
        for (int i = 0; i < v1.length(); i++)
        {
            new_data[i] = v1[i] + v2[i];
        }
        return Vector(v1.length(), new_data);
    }

    Vector v_diff(const VectorLike &v1, const VectorLike &v2)
    {
        if(v1.length() != v2.length()) throw std::invalid_argument("Cannot subtract vectors of different sizes");
        double *new_data = new double[v1.length()];
        for (int i = 0; i < v1.length(); i++)
        {
            new_data[i] = v1[i] - v2[i];
        }
        return Vector(v1.length(), new_data);
    }

}