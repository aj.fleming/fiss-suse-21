#ifndef FISS_MATRIX_H
#define FISS_MATRIX_H

#include "vector.hpp"
namespace fiss
{

    class Matrix;
    class MatrixView;

    class MatrixLike
    {
    private:
        int m_rows, m_cols;

    protected:
        void set_rows(const int t_rows);
        void set_cols(const int t_cols);

    public:
        MatrixLike(int t_rows, int t_cols);
        virtual double &operator()(int r, int c) = 0;
        virtual double operator()(int r, int c) const = 0;

        int rows() const;
        int cols() const;

        Matrix operator*(const double s) const;

        virtual Vector dot(const VectorLike &v) const;

        virtual Matrix dot(const MatrixLike &m) const;
    };
    // Standard representation of a dense matrix of doubles
    class Matrix : public MatrixLike
    {
    private:
        // pointer to the matrix data, size of the array is m_cols*m_rows doubles
        double *m_data;

    public:
        Matrix();
        Matrix(const Matrix &m);
        Matrix(Matrix &&m);
        // constructor of an empty matrix
        // initializes all values to 0
        Matrix(int t_rows, int t_cols);
        // constructor of a matrix with a row-major data array provided
        Matrix(int t_rows, int t_cols, double *t_data);
        // destroy a matrix
        ~Matrix();
        // deep copy assignment
        Matrix &operator=(const Matrix &m);
        // shallow copy / move assigment
        Matrix &operator=(Matrix &&m);

        // access an element at [r,c]
        double operator()(int r, int c) const;
        double &operator()(int r, int c);

        // set a row to some vector v
        void set_row(int r, const VectorLike &v);
        // set a column to some vector v
        void set_col(int c, const VectorLike &v);

        VectorView get_col(int c);
        VectorView get_row(int r);
        MatrixView submatrix(int r_start, int c_start, int t_rows, int t_cols);
        MatrixView T();
    };

    class MatrixView : public MatrixLike
    {
        private:
        double* m_data_start; 
        // m_col_step is the step size between elements in a row (usually 1)
        // named this way because it "steps between" columns
        int m_col_step;
        //m_row_step is the step size between elements in a column (usually the row length of a row-major matrix)
        int m_row_step;
        public:
        MatrixView(int t_rows, int t_cols, double* t_data, int t_row_stride, int t_col_stride);
        MatrixView(const MatrixView &mv);
        MatrixView &operator=(const MatrixView &mv);
        double operator()(int r, int c) const;
        double &operator()(int r, int c);
    };

    Matrix eye(int dim);
    Matrix givens(const Matrix& a, int j);
};
#endif