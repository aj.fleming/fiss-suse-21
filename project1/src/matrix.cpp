#include <stdexcept>
#include <cmath>

#include "fiss.hpp"
#include "matrix.hpp"
#include "vector.hpp"

namespace fiss
{
    MatrixLike::MatrixLike(int t_rows, int t_cols) : m_rows{t_rows}, m_cols{t_cols} {}

    void MatrixLike::set_cols(int c)
    {
        m_cols = c;
    }

    void MatrixLike::set_rows(int r)
    {
        m_rows = r;
    }

    int MatrixLike::rows() const { return m_rows; }
    int MatrixLike::cols() const { return m_cols; }

    Matrix MatrixLike::operator*(const double s) const
    {
        Matrix m(m_rows, m_cols);
        for (int i = 0; i < m_rows; i++)
        {
            for (int j = 0; j < m_cols; j++)
            {
                m(i, j) = (*this)(i, j);
            }
        }
        return m;
    }

    Vector MatrixLike::dot(const VectorLike &v) const
    {
        if (v.length() != m_cols)
            throw std::invalid_argument("vector length not equal to matrix width");
        Vector v1(m_rows);
        for (int i = 0; i < m_rows; i++)
        {
            for (int j = 0; j < m_cols; j++)
            {
                v1[i] = v1[i] + (*this)(i, j) * v[j];
            }
        }
        return v1;
    }

    Matrix MatrixLike::dot(const MatrixLike &m) const
    {
        if (m_cols != m.m_rows)
            throw std::invalid_argument("dimension mismatch in matrix dot product");
        Matrix result(m_rows, m.m_cols);
        for (int i = 0; i < result.m_rows; i++)
        {
            for (int j = 0; j < result.m_cols; j++)
            {
                //a_ij = i'th row of first .* j'th col of second
                for(int k = 0; k < m_cols; k++){
                    result(i, j) += (*this)(i,k)*m(k,j);
                }
            }
        }
        return result;
    }

    Matrix::Matrix() : MatrixLike{0, 0}, m_data{nullptr}
    {
    }

    Matrix::Matrix(int t_rows, int t_cols) : MatrixLike{t_rows, t_cols}
    {
        m_data = new double[t_rows * t_cols];
        for (int i = 0; i < t_rows * t_cols; i++)
        {
            m_data[i] = 0;
        }
    }

    Matrix::Matrix(int t_rows, int t_cols, double *t_data) : MatrixLike{t_rows, t_cols}, m_data{t_data}
    {
    }

    Matrix::Matrix(const Matrix &m) : MatrixLike{m.rows(), m.cols()}
    {
        m_data = new double[rows() * cols()];
        for (int i = 0; i < rows() * cols(); i++)
        {
            m_data[i] = m.m_data[i];
        }
    }

    Matrix::Matrix(Matrix &&m) : MatrixLike{m.rows(), m.cols()}, m_data{m.m_data}
    {
        m.m_data = nullptr;
    }

    Matrix::~Matrix()
    {
        delete[] m_data;
    }

    Matrix &Matrix::operator=(const Matrix &m)
    {
        if (&m == this)
        {
            return *this;
        }
        delete[] m_data;
        set_rows(m.rows());
        set_cols(m.cols());
        m_data = new double[rows() * cols()];
        for (int i = 0; i < rows() * cols(); i++)
        {
            m_data[i] = m.m_data[i];
        }

        return *this;
    }

    Matrix &Matrix::operator=(Matrix &&m)
    {
        if (&m == this)
        {
            return *this;
        }
        delete[] m_data;
        set_rows(m.rows());
        set_cols(m.cols());
        m_data = m.m_data;
        m.m_data = nullptr;
        return *this;
    }

    double Matrix::operator()(int r, int c) const
    {
        return m_data[cols() * r + c];
    }

    double &Matrix::operator()(int r, int c)
    {
        return m_data[cols() * r + c];
    }

    VectorView Matrix::get_col(int c)
    {
        return VectorView(rows(), cols(), &m_data[c]);
    }

    VectorView Matrix::get_row(int r)
    {
        return VectorView(cols(), 1, &m_data[r * cols()]);
    }

    void Matrix::set_row(int r, const VectorLike &v)
    {
        if (v.length() != cols())
            throw std::invalid_argument("Vector length does not match row length");
        if (r >= rows())
            throw std::invalid_argument("Cannot set row that does not exist");

        for (int i = 0; i < cols(); i++)
        {
            m_data[r * cols() + i] = v[i];
        }
    }

    void Matrix::set_col(int c, const VectorLike &v)
    {
        if (v.length() != rows())
            throw std::invalid_argument("Vector length does not match column length");
        if (c >= cols())
            throw std::invalid_argument("Cannot set column that does not exist");

        for (int i = 0; i < rows(); i++)
        {
            m_data[i * cols() + c] = v[i];
        }
    }

    MatrixView Matrix::submatrix(int r_start, int c_start, int t_rows, int t_cols)
    {
        return MatrixView(t_rows, t_cols, &m_data[cols() * r_start + c_start], 1, cols());
    }

    MatrixView Matrix::T(){
        return MatrixView(cols(), rows(), m_data, cols(), 1);
    }

    Matrix givens(const Matrix &a, int j){
        double mag = std::hypot(a(j,j), a(j+1, j));
        double c = a(j,j)/mag;
        double s = a(j+1, j)/mag;
        Matrix g(eye(a.rows()));
        g(j,j) = c;
        g(j+1, j+1) = c;
        g(j, j+1) = s;
        g(j+1, j) = -s;
        return g;
    }

    MatrixView::MatrixView(int t_rows, int t_cols, double *t_data, int t_row_stride, int t_col_stride)
        : MatrixLike{t_rows, t_cols}, m_data_start{t_data}, m_col_step{t_row_stride}, m_row_step{t_col_stride}
    {
    }
    MatrixView::MatrixView(const MatrixView &mv) : MatrixLike{mv.rows(), mv.cols()}, m_data_start{mv.m_data_start}, m_col_step{mv.m_col_step}, m_row_step{mv.m_row_step} {}

    MatrixView &MatrixView::operator=(const MatrixView &mv)
    {
        if (&mv == this)
        {
            return *this;
        }
        set_cols(mv.cols());
        set_rows(mv.rows());
        m_data_start = mv.m_data_start;
        m_col_step = mv.m_col_step;
        m_row_step = mv.m_row_step;
        return *this;
    }

    double MatrixView::operator()(int i, int j) const
    {
        return m_data_start[i * m_row_step + j * m_col_step];
    }

    double &MatrixView::operator()(int i, int j)
    {
        return m_data_start[i * m_row_step + j * m_col_step];
    }

    Matrix eye(int dim)
    {
        Matrix i(dim, dim);
        for (int j = 0; j < dim; j++)
        {
            i(j, j) = 1;
        }
        return i;
    }



}