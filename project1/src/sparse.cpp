#include <stdexcept>
#include <cmath>
#include <iostream>

#include "vector.hpp"
#include "matrix.hpp"
#include "sparse.hpp"

namespace fiss
{

    SparseDiag::SparseDiag(unsigned t_dim, double *t_data) : m_dim{t_dim}, m_data{t_data} {}

    SparseDiag::SparseDiag(SparseDiag &&s) : m_dim{s.m_dim}, m_data{s.m_data}
    {
        s.m_data = nullptr;
    }

    SparseDiag::~SparseDiag()
    {
        delete[] m_data;
    }

    Vector SparseDiag::dot(const VectorLike &v) const
    {
        if (v.length() != m_dim)
            throw std::invalid_argument("vector length does not match diagonal matrix dimension");
        Vector result(m_dim);
        for (int i = 0; i < m_dim; i++)
        {
            result[i] = v[i] * m_data[i];
        }
        return result;
    }

    SparseCSR SparseDiag::dot(const SparseCSR &s) const
    {
        unsigned *row_idx = new unsigned[s.rows() + 1];
        unsigned *col_idx = new unsigned[s.m_nonzero];
        double *data = new double[s.m_nonzero];

        for (int i = 0; i < s.rows(); i++)
        {
            row_idx[i] = s.m_row_start_idx[i];
            for (int k = s.m_row_start_idx[i]; k < s.m_row_start_idx[i + 1]; k++)
            {
                col_idx[k] = s.m_col_idx[k];
                data[k] = m_data[i] * s.m_data[k];
            }
        }
        row_idx[s.rows()] = s.m_row_start_idx[s.rows()];
        return SparseCSR(s.rows(), s.cols(), s.nonzero_elements(), row_idx, col_idx, data);
    }

    SparseDiag SparseDiag::invert() const
    {
        double *inv = new double[m_dim];
        for (int i = 0; i < m_dim; i++)
        {
            inv[i] = 1 / m_data[i];
        }

        return SparseDiag(m_dim, inv);
    }

    double SparseDiag::operator[](unsigned i) const
    {
        return m_data[i];
    }

    SparseCSR::SparseCSR(unsigned t_rows, unsigned t_cols, unsigned t_nonzero,
                         unsigned *t_row_start, unsigned *t_col_idx, double *t_data)
        : m_rows{t_rows}, m_cols{t_cols}, m_nonzero{t_nonzero},
          m_row_start_idx{t_row_start}, m_col_idx{t_col_idx}, m_data{t_data}
    {
    }

    SparseCSR::SparseCSR(const SparseCSR &s)
    {
    }

    SparseCSR::SparseCSR(SparseCSR &&s) : m_rows{s.m_rows}, m_cols{s.m_cols}, m_nonzero{s.m_nonzero},
                                          m_row_start_idx{s.m_row_start_idx}, m_col_idx{s.m_col_idx}, m_data{s.m_data}
    {
        s.m_data = nullptr;
        s.m_col_idx = nullptr;
        s.m_row_start_idx = nullptr;
    }

    SparseCSR::~SparseCSR()
    {
        delete m_row_start_idx;
        delete m_col_idx;
        delete m_data;
    }

    unsigned SparseCSR::rows() const
    {
        return m_rows;
    }

    unsigned SparseCSR::cols() const
    {
        return m_cols;
    }

    unsigned SparseCSR::nonzero_elements() const
    {
        return m_nonzero;
    }

    Vector SparseCSR::dot(const VectorLike &v) const
    {
        if (v.length() != m_cols)
            throw std::invalid_argument("Vector length not equal to sparse matrix width");
        Vector result(m_rows);
        for (int i = 0; i < m_rows; i++)
        {
            for (int j = m_row_start_idx[i]; j < m_row_start_idx[i + 1]; j++)
            {
                result[i] = result[i] + m_data[j] * v[m_col_idx[j]];
            }
        }
        return result;
    }

    Vector SparseCSR::diag() const
    {
        Vector v(m_rows);
        for (int i = 0; i < m_rows; i++)
        {
            for (int k = m_row_start_idx[i]; k < m_row_start_idx[i + 1]; k++)
            {
                unsigned j = m_col_idx[k];
                if (i == j)
                {
                    v[i] = m_data[k];
                }
            }
        }
        return v;
    }

    SparseCSR SparseCSR::E() const
    {
        // determine how many elements are in E
        unsigned count = 0;
        unsigned* new_row_starts = new unsigned[m_rows+1];
        new_row_starts[0] = 0;
        for (int i = 0; i < m_rows; i++)
        {
            new_row_starts[i+1] = new_row_starts[i];
            for (int k = m_row_start_idx[i]; k < m_row_start_idx[i + 1]; k++)
            {
                if (m_col_idx[k] <= i)
                {
                    count++;
                    new_row_starts[i+1]++;
                }
            }
        }
        unsigned* new_col_idx = new unsigned[count];
        double* new_data = new double[count];
        unsigned el_idx = 0;
        for (int i = 0; i < m_rows; i++)
        {
            for (int k = m_row_start_idx[i]; k < m_row_start_idx[i + 1]; k++)
            {
                if (m_col_idx[k] <= i)
                {
                    new_col_idx[el_idx] = m_col_idx[k];
                    new_data[el_idx] = m_data[k];
                    el_idx++; 
                }
            }
        }
        return SparseCSR(m_rows, m_cols, count, new_row_starts, new_col_idx, new_data);
    }

    SparseCSRSymmetric::SparseCSRSymmetric(unsigned t_dim, unsigned t_nonzero,
                                           unsigned *t_row_start, unsigned *t_col_idx, double *t_data)
        : SparseCSR{t_dim, t_dim, t_nonzero,
                    t_row_start, t_col_idx, t_data}
    {
    }

    SparseCSRSymmetric::SparseCSRSymmetric(const SparseCSRSymmetric &ss) : SparseCSR{ss}
    {
    }

    SparseCSRSymmetric::SparseCSRSymmetric(SparseCSRSymmetric &&ss) : SparseCSR{ss}
    {
    }

    SparseCSRSymmetric::~SparseCSRSymmetric()
    {
        delete m_col_idx;
        delete m_row_start_idx;
        delete m_data;
    }

    Vector SparseCSRSymmetric::dot(const VectorLike &v) const
    {
        if (v.length() != m_cols)
            throw std::invalid_argument("Vector length does not match symmetric matrix width");
        Vector result(m_rows);
        for (unsigned i = 0; i < m_rows; i++)
        {
            for (unsigned c = m_row_start_idx[i]; c < m_row_start_idx[i + 1]; c++)
            {
                unsigned j = m_col_idx[c];
                // Aij = Aji
                double Aij = m_data[c];
                result[i] = result[i] + Aij * v[j];
                // handle the symmetric case
                if (i != j)
                {
                    result[j] = result[j] + Aij * v[i];
                }
            }
        }
        return result;
    }

    SparseDiag extract_diagonal(const SparseCSR *m)
    {
        double *diag = new double[m->rows()];
        for (unsigned i = 0; i < m->rows(); i++)
        {
            diag[i] = 0;
            for (unsigned k = i; k < m->m_row_start_idx[i + 1]; k++)
            {
                unsigned j = m->m_col_idx[k];
                if (i == j)
                    diag[i] = m->m_data[j];
            }
        }
        return SparseDiag(m->rows(), diag);
    }

}