#ifndef FISS_H
#define FISS_H
#define FISS_GMRES_MAX_ITER 500
#define FISS_MAX_KRYLOV_SIZE 750
#define FISS_CG_MAX_ITER 1000000
#define FISS_PRECOND_NONE 0
#define FISS_PRECOND_JACOBI 1
#define FISS_PRECOND_GS 2
#include <string>

#include "vector.hpp"
#include "matrix.hpp"
#include "sparse.hpp"

namespace fiss
{

    class CSR_Matrix
    {
    public:
        int m_rows, m_cols, m_non_zero_elements;
        // row_start_idx holds start indices for each row
        // i.e. [0, 4, 7] indicates a 2-row matrix with data elements 0,1,2,3 in row 0
        // the length of this vector is m_rows+1, to allow computation of the length of the last row
        //  (at the cost of 1 int of storage)
        int *m_row_start_idx;
        // col_idx holds the column indices of each data element
        // length m_non_zero_elements
        int *m_col_idx;
        // nonzero data elements in the matrix
        double *m_data;
        //public:
        CSR_Matrix(int r, int c, int n_nz, int *r_end, int *c_idx, double *d);
        Vector dot(const VectorLike &v) const;
        ~CSR_Matrix();
    };

    void pretty_print_matrix(MatrixLike &m);
    void pretty_print_vector(VectorLike &v);

    // Load a matrix from a Matrix Market type file. Will return a matrix in CSR format.
    CSR_Matrix load_matrix(std::string path);
    SparseCSR* load_from_mm_file(const char *path);

    // GMRES Algorithm
    // Uses the Modified Gram-Schmidt prodedure (Modified Arnoldi) see Yousef Saad. Iterative Methods for Sparse Linear Systems. 2003. p148
    // as well as Lectures 3 and 4 from FISS21
    // Solve Ax = b, initial guess x0, solution dimension n, krylov space dimension m, solution tolerance tol
    Vector restarted_gmres(SparseCSR *A, const Vector &b, Vector &x0, const int n, int m, double tol, std::string out, int precond_type);
    Vector restarted_gmres_jacobi(SparseCSR *A, const Vector &b, Vector &x0, const int n, int m, double tol, std::string out, int precond_type);
    Vector restarted_gmres_gauss(SparseCSR *A, const Vector &b, Vector &x0, const int n, int m, double tol, std::string out, int precond_type);
    // gram-schmidt procedure to generate orthogonal basis matrix Vbar and upper Hessenberg matrix Hbar
    void gram_schmidt(Matrix &Vbar, Matrix &Hbar, const CSR_Matrix &A, const VectorLike &r0, int m);
    // eliminate the subdiag of an upper Hessenberg matrix Hbar and fill in vector g to find y_opt
    void eliminate_subdiag_and_fill_g(Matrix &Hbar, Vector &g);

    // Conjugate Gradient
    Vector conjugate_gradient(SparseLike *A, const Vector &b, const Vector &x0, const Vector &known_x, const double TOL, std::string out);

    Vector full_gmres(SparseCSR *A, const Vector &b, const Vector &x0, const double TOL, std::string out);
    Vector full_gmres_jacobi(SparseCSR *A, const Vector &b, const Vector &x0, const double TOL, std::string out);
    Vector full_gmres_gauss(SparseCSR *A, const Vector &b, const Vector &x0, const double TOL, std::string out);
}
#endif