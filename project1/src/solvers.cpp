#include <cmath>
#include <vector>
#include <iostream>
#include <fstream>
#include <chrono>

#include "fiss.hpp"
#include "vector.hpp"
#include "matrix.hpp"
#include "sparse.hpp"

namespace fiss
{

    void extend_krylov_space_basis(const SparseLike *A, Matrix &krylov_basis, Matrix &h, int j)
    {
        Vector w(A->dot(krylov_basis.get_col(j)));
        for (int i = 0; i <= j; i++)
        {
            VectorView v_i(krylov_basis.get_col(i));
            h(i, j) = v_i.dot(w);
            w -= v_i * h(i, j);
        }
        h(j + 1, j) = w.norm();
        krylov_basis.set_col(j + 1, w * (1 / h(j + 1, j)));
    }

    // solve upper-triangular Ax = b and return x
    Vector solve_via_back_substitution(MatrixLike &A, VectorLike &b)
    {
        Vector x(b.length());
        double temp;
        for (int i = A.rows() - 1; i > -1; i--)
        {
            temp = 0;
            for (int j = i + 1; j < A.cols(); j++)
            {
                temp = temp + A(i, j) * x[j];
            }
            x[i] = (b[i] - temp) / A(i, i);
        }
        return x;
    }

    //solve Mx = w via forward-sub (M lower-triangular)
    Vector precondition_via_forward_sub(SparseCSR &M, Vector &w)
    {
        Vector x(w.length());
        for (int i = 0; i < w.length(); i++)
        {
            double temp = 0;
            int k = M.m_row_start_idx[i];
            while (k < M.m_row_start_idx[i + 1] - 1)
            {
                temp += M.m_data[k] * w[M.m_col_idx[k]];
                k++;
            }
            x[i] = (w[i] - temp) / M.m_data[k];
        }
        return x;
    }

    Vector restarted_gmres(SparseCSR *A, const Vector &b, Vector &x0, const int n, int m, double tol, std::string out, int precond_type)
    {
        std::cout << "Beginning restarted GMRES with m=" << m << std::endl;
        std::ofstream datafile;
        datafile.open(out);
        datafile << "problem_size = " << n << std::endl
                 << "krylov_size = " << m << std::endl;
        datafile << "relative_tol = " << tol << std::endl;
        // set up for gmres algorithm:
        // vector to track our current solution
        Vector x(x0);

        // outer loop
        // based on the size of the relative residual
        int step = 0;

        // this is bad and evil
        Vector r0 = b - A->dot(x0);
        double rho_0 = r0.norm();
        std::cout << "rho_0 = " << rho_0 << std::endl;
        datafile << "rho_0 = " << rho_0 << std::endl;
        datafile << "iter_data = [";
        double residual;
        do
        {
            auto step_start = std::chrono::high_resolution_clock::now();

            Vector cur_res(b - A->dot(x));
            double res = cur_res.norm();
            Matrix krylov_basis(n, m + 1);
            krylov_basis.set_col(0, (cur_res) * (1 / res));
            Matrix H_bar(m + 1, m);
            // Vectors to hold givens rotations and residual
            Vector c(m), s(m), g(m + 1);
            g[0] = res;
            for (int j = 0; j < m; j++)
            {
                // std::cout << "extending basis to column " << j << std::endl;
                extend_krylov_space_basis(A, krylov_basis, H_bar, j);

                // Apply all previous givens rotations
                double t1, t2, t3;
                for (int k = 0; k < j; k++)
                {
                    t1 = c[k] * H_bar(k, j) + s[k] * H_bar(k + 1, j);
                    t2 = -s[k] * H_bar(k, j) + c[k] * H_bar(k + 1, j);
                    H_bar(k, j) = t1;
                    H_bar(k + 1, j) = t2;
                }
                t3 = std::hypot(H_bar(j, j), H_bar(j + 1, j));
                c[j] = H_bar(j, j) / t3;
                s[j] = H_bar(j + 1, j) / t3;
                t1 = c[j] * H_bar(j, j) + s[j] * H_bar(j + 1, j);
                t2 = -s[j] * H_bar(j, j) + c[j] * H_bar(j + 1, j);
                H_bar(j, j) = t1;
                H_bar(j + 1, j) = t2;
                // pretty_print_matrix(H_bar);
                // Generate G_{j,j+1}
                g[j + 1] = -s[j] * g[j];
                g[j] = c[j] * g[j];
            }

            MatrixView Rm(H_bar.submatrix(0, 0, m, m));
            MatrixView Vm(krylov_basis.submatrix(0, 0, n, m));
            VectorView g_star(g.slice(0, m));
            Vector y_opt(solve_via_back_substitution(Rm, g_star));
            x += Vm.dot(y_opt);
            residual = std::abs(g[m]);

            auto step_end = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> elapsed = step_end - step_start;
            datafile << "{ 'iter':" << m * step << ", 'duration':" << elapsed.count() << ", 'rho':" << residual << ", 'mag_x':" << x.norm() << "},";
            step++;
        } while ((residual / rho_0 > tol) && step < FISS_GMRES_MAX_ITER);
        datafile << "]" << std::endl;
        datafile.close();
        std::cout << "Restarted GMRES completed after " << step << " iterations" << std::endl;
        return x;
    }

    Vector restarted_gmres_jacobi(SparseCSR *A, const Vector &b, Vector &x0, const int n, int m, double tol, std::string out, int precond_type)
    {
        std::cout << "Beginning restarted GMRES with Jacobi preconditioning, m=" << m << std::endl;
        std::ofstream datafile;
        datafile.open(out);
        datafile << "problem_size = " << n << std::endl
                 << "krylov_size = " << m << std::endl;
        datafile << "relative_tol = " << tol << std::endl;
        // set up for gmres algorithm:
        // vector to track our current solution
        Vector x(x0);

        // outer loop
        // based on the size of the relative residual
        int step = 0;

        //compute M^-1 = D^-1
        Vector A_diag(A->diag());
        Vector M_inv(A_diag.length());
        std::cout << A_diag.length();
        for (int i = 0; i < A_diag.length(); i++)
        {
            M_inv[i] = 1 / A_diag[i];
        }
        Vector temp(b - A->dot(x0));
        std::cout << std::endl
                  << temp.norm() << std::endl;
        Vector r(M_inv * temp);
        double rho_0 = r.norm();
        std::cout << "rho_0 = " << rho_0 << std::endl;
        datafile << "rho_0 = " << rho_0 << std::endl;
        datafile << "iter_data = [";
        double residual;
        auto step_start = std::chrono::high_resolution_clock::now();
        do
        {
            Vector cur_res(M_inv * (b - A->dot(x)));
            step_start = std::chrono::high_resolution_clock::now();
            Matrix krylov_basis(n, m + 1);
            krylov_basis.set_col(0, cur_res * (1 / cur_res.norm()));
            Vector g(m + 1);
            g[0] = cur_res.norm();
            Matrix H_bar(m + 1, m);
            // Vectors to hold givens rotations and residual
            Vector c(m), s(m);

            for (int j = 0; j < m; j++)
            {
                //extend krylov basis w/ jacobi precond
                Vector w(M_inv * (A->dot(krylov_basis.get_col(j))));
                for (int i = 0; i <= j; i++)
                {
                    VectorView v_i(krylov_basis.get_col(i));
                    H_bar(i, j) = v_i.dot(w);
                    w -= v_i * H_bar(i, j);
                }
                H_bar(j + 1, j) = w.norm();
                krylov_basis.set_col(j + 1, w * (1 / H_bar(j + 1, j)));

                // Apply all previous givens rotations
                double t1, t2, t3;
                for (int k = 0; k < j; k++)
                {
                    t1 = c[k] * H_bar(k, j) + s[k] * H_bar(k + 1, j);
                    t2 = -s[k] * H_bar(k, j) + c[k] * H_bar(k + 1, j);
                    H_bar(k, j) = t1;
                    H_bar(k + 1, j) = t2;
                }
                t3 = std::hypot(H_bar(j, j), H_bar(j + 1, j));
                c[j] = H_bar(j, j) / t3;
                s[j] = H_bar(j + 1, j) / t3;
                t1 = c[j] * H_bar(j, j) + s[j] * H_bar(j + 1, j);
                t2 = -s[j] * H_bar(j, j) + c[j] * H_bar(j + 1, j);
                H_bar(j, j) = t1;
                H_bar(j + 1, j) = t2;
                // pretty_print_matrix(H_bar);
                // Generate G_{j,j+1}
                g[j + 1] = -s[j] * g[j];
                g[j] = c[j] * g[j];
            }

            MatrixView Rm(H_bar.submatrix(0, 0, m, m));
            MatrixView Vm(krylov_basis.submatrix(0, 0, n, m));
            VectorView g_star(g.slice(0, m));
            Vector y_opt(solve_via_back_substitution(Rm, g_star));
            x += Vm.dot(y_opt);
            residual = std::abs(g[m]);

            auto step_end = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> elapsed = step_end - step_start;
            datafile << "{ 'iter':" << m * step << ", 'duration':" << elapsed.count() << ", 'rho':" << residual << ", 'mag_x':" << x.norm() << "},";
            step++;
        } while ((residual / rho_0 > tol) && step < FISS_GMRES_MAX_ITER);
        datafile << "]" << std::endl;
        datafile.close();
        std::cout << "Restarted GMRES completed after " << step << " iterations" << std::endl;
        return x;
    }

    Vector restarted_gmres_gauss(SparseCSR *A, const Vector &b, Vector &x0, const int n, int m, double tol, std::string out, int precond_type)
    {
        std::cout << "Beginning restarted GMRES with Jacobi preconditioning, m=" << m << std::endl;
        std::ofstream datafile;
        datafile.open(out);
        datafile << "problem_size = " << n << std::endl
                 << "krylov_size = " << m << std::endl;
        datafile << "relative_tol = " << tol << std::endl;
        // set up for gmres algorithm:
        // vector to track our current solution
        Vector x(x0);

        // outer loop
        // based on the size of the relative residual
        int step = 0;

        SparseCSR E(A->E());
        Vector temp(b - A->dot(x0));
        std::cout << std::endl
                  << temp.norm() << std::endl;
        Vector r(precondition_via_forward_sub(E, temp));
        double rho_0 = r.norm();
        std::cout << "rho_0 = " << rho_0 << std::endl;
        datafile << "rho_0 = " << rho_0 << std::endl;
        datafile << "iter_data = [";
        double residual;
        auto step_start = std::chrono::high_resolution_clock::now();
        do
        {
            Vector temp(b - A->dot(x));
            Vector cur_res(precondition_via_forward_sub(E, temp));
            step_start = std::chrono::high_resolution_clock::now();
            Matrix krylov_basis(n, m + 1);
            krylov_basis.set_col(0, cur_res * (1 / cur_res.norm()));
            Vector g(m + 1);
            g[0] = cur_res.norm();
            Matrix H_bar(m + 1, m);
            // Vectors to hold givens rotations and residual
            Vector c(m), s(m);

            for (int j = 0; j < m; j++)
            {
                //extend krylov basis w/ jacobi precond
                Vector temp(A->dot(krylov_basis.get_col(j)));
                Vector w(precondition_via_forward_sub(E, temp));
                for (int i = 0; i <= j; i++)
                {
                    VectorView v_i(krylov_basis.get_col(i));
                    H_bar(i, j) = v_i.dot(w);
                    w -= v_i * H_bar(i, j);
                }
                H_bar(j + 1, j) = w.norm();
                krylov_basis.set_col(j + 1, w * (1 / H_bar(j + 1, j)));

                // Apply all previous givens rotations
                double t1, t2, t3;
                for (int k = 0; k < j; k++)
                {
                    t1 = c[k] * H_bar(k, j) + s[k] * H_bar(k + 1, j);
                    t2 = -s[k] * H_bar(k, j) + c[k] * H_bar(k + 1, j);
                    H_bar(k, j) = t1;
                    H_bar(k + 1, j) = t2;
                }
                t3 = std::hypot(H_bar(j, j), H_bar(j + 1, j));
                c[j] = H_bar(j, j) / t3;
                s[j] = H_bar(j + 1, j) / t3;
                t1 = c[j] * H_bar(j, j) + s[j] * H_bar(j + 1, j);
                t2 = -s[j] * H_bar(j, j) + c[j] * H_bar(j + 1, j);
                H_bar(j, j) = t1;
                H_bar(j + 1, j) = t2;
                // pretty_print_matrix(H_bar);
                // Generate G_{j,j+1}
                g[j + 1] = -s[j] * g[j];
                g[j] = c[j] * g[j];
            }

            MatrixView Rm(H_bar.submatrix(0, 0, m, m));
            MatrixView Vm(krylov_basis.submatrix(0, 0, n, m));
            VectorView g_star(g.slice(0, m));
            Vector y_opt(solve_via_back_substitution(Rm, g_star));
            x += Vm.dot(y_opt);
            residual = std::abs(g[m]);

            auto step_end = std::chrono::high_resolution_clock::now();
            std::chrono::duration<double> elapsed = step_end - step_start;
            datafile << "{ 'iter':" << m * step << ", 'duration':" << elapsed.count() << ", 'rho':" << residual << ", 'mag_x':" << x.norm() << "},";
            step++;
        } while ((residual / rho_0 > tol) && step < FISS_GMRES_MAX_ITER);
        datafile << "]" << std::endl;
        datafile.close();
        std::cout << "Restarted GMRES completed after " << step << " iterations" << std::endl;
        return x;
    }

    Vector full_gmres(SparseCSR *A, const Vector &b, const Vector &x0, const double TOL, std::string out)
    {
        std::cout << "Beginning full GMRES" << std::endl;
        std::ofstream datafile;
        datafile.open(out);
        datafile << "problem_size = " << b.length() << std::endl;
        datafile << "relative_tol = " << TOL << std::endl;

        Vector r(b - A->dot(x0));
        double rho_0 = r.norm();
        std::cout << "rho_0 = " << rho_0 << std::endl;
        datafile << "rho_0 = " << rho_0 << std::endl;

        Matrix krylov_basis(b.length(), FISS_MAX_KRYLOV_SIZE + 1);
        krylov_basis.set_col(0, r * (1 / rho_0));
        Matrix H_bar(FISS_MAX_KRYLOV_SIZE + 1, FISS_MAX_KRYLOV_SIZE);
        Vector c(FISS_MAX_KRYLOV_SIZE), s(FISS_MAX_KRYLOV_SIZE), g(FISS_MAX_KRYLOV_SIZE + 1);
        g[0] = rho_0;
        int j = 0;
        datafile << "residuals = [";
        do
        {
            extend_krylov_space_basis(A, krylov_basis, H_bar, j);
            // Apply all previous givens rotations
            double t1, t2, t3;
            for (int k = 0; k < j; k++)
            {
                t1 = c[k] * H_bar(k, j) + s[k] * H_bar(k + 1, j);
                t2 = -s[k] * H_bar(k, j) + c[k] * H_bar(k + 1, j);
                H_bar(k, j) = t1;
                H_bar(k + 1, j) = t2;
            }
            t3 = std::hypot(H_bar(j, j), H_bar(j + 1, j));
            c[j] = H_bar(j, j) / t3;
            s[j] = H_bar(j + 1, j) / t3;
            t1 = c[j] * H_bar(j, j) + s[j] * H_bar(j + 1, j);
            t2 = -s[j] * H_bar(j, j) + c[j] * H_bar(j + 1, j);
            H_bar(j, j) = t1;
            H_bar(j + 1, j) = t2;
            // pretty_print_matrix(H_bar);
            // Generate G_{j,j+1}
            g[j + 1] = -s[j] * g[j];
            g[j] = c[j] * g[j];
            datafile << std::abs(g[j + 1]) / rho_0 << ", ";
            j++;
        } while (std::abs(g[j] / rho_0) > TOL && j < FISS_MAX_KRYLOV_SIZE);
        datafile << "]" << std::endl;
        datafile.close();

        MatrixView Rm(H_bar.submatrix(0, 0, j, j));
        MatrixView Vm(krylov_basis.submatrix(0, 0, b.length(), j));
        VectorView g_star(g.slice(0, j));
        Vector y_opt(solve_via_back_substitution(Rm, g_star));
        return x0 + Vm.dot(y_opt);
    }

    Vector full_gmres_jacobi(SparseCSR *A, const Vector &b, const Vector &x0, const double TOL, std::string out)
    {
        std::cout << "Beginning full GMRES with Jacobi preconditioning" << std::endl;
        std::ofstream datafile;
        datafile.open(out);
        datafile << "problem_size = " << b.length() << std::endl;
        datafile << "relative_tol = " << TOL << std::endl;

        //compute M^-1 = D^-1
        Vector A_diag(A->diag());
        Vector M_inv(A_diag.length());
        std::cout << A_diag.length();
        for (int i = 0; i < A_diag.length(); i++)
        {
            M_inv[i] = 1 / A_diag[i];
        }
        Vector temp(b - A->dot(x0));
        std::cout << std::endl
                  << temp.norm() << std::endl;
        Vector r(M_inv * temp);
        double rho_0 = r.norm();
        std::cout << "rho_0 = " << rho_0 << std::endl;
        datafile << "rho_0 = " << rho_0 << std::endl;

        Matrix krylov_basis(b.length(), FISS_MAX_KRYLOV_SIZE + 1);
        krylov_basis.set_col(0, r * (1 / rho_0));
        Matrix H_bar(FISS_MAX_KRYLOV_SIZE + 1, FISS_MAX_KRYLOV_SIZE);
        Vector c(FISS_MAX_KRYLOV_SIZE), s(FISS_MAX_KRYLOV_SIZE), g(FISS_MAX_KRYLOV_SIZE + 1);
        g[0] = rho_0;
        int j = 0;
        datafile << "residuals = [";
        std::cout << std::endl
                  << "beginning main loop" << std::endl;
        do
        {
            //extend krylov basis w/ jacobi precond
            Vector w(M_inv * (A->dot(krylov_basis.get_col(j))));
            for (int i = 0; i <= j; i++)
            {
                VectorView v_i(krylov_basis.get_col(i));
                H_bar(i, j) = v_i.dot(w);
                w -= v_i * H_bar(i, j);
            }
            H_bar(j + 1, j) = w.norm();
            krylov_basis.set_col(j + 1, w * (1 / H_bar(j + 1, j)));
            // Apply all previous givens rotations
            double t1, t2, t3;
            for (int k = 0; k < j; k++)
            {
                t1 = c[k] * H_bar(k, j) + s[k] * H_bar(k + 1, j);
                t2 = -s[k] * H_bar(k, j) + c[k] * H_bar(k + 1, j);
                H_bar(k, j) = t1;
                H_bar(k + 1, j) = t2;
            }
            t3 = std::hypot(H_bar(j, j), H_bar(j + 1, j));
            c[j] = H_bar(j, j) / t3;
            s[j] = H_bar(j + 1, j) / t3;
            t1 = c[j] * H_bar(j, j) + s[j] * H_bar(j + 1, j);
            t2 = -s[j] * H_bar(j, j) + c[j] * H_bar(j + 1, j);
            H_bar(j, j) = t1;
            H_bar(j + 1, j) = t2;
            // pretty_print_matrix(H_bar);
            // Generate G_{j,j+1}
            g[j + 1] = -s[j] * g[j];
            g[j] = c[j] * g[j];
            datafile << std::abs(g[j + 1]) / rho_0 << ", ";
            j++;
            // limit of 600 just in case
        } while (std::abs(g[j] / rho_0) > TOL && j < 600);
        datafile << "]" << std::endl;
        datafile.close();

        MatrixView Rm(H_bar.submatrix(0, 0, j, j));
        MatrixView Vm(krylov_basis.submatrix(0, 0, b.length(), j));
        VectorView g_star(g.slice(0, j));
        Vector y_opt(solve_via_back_substitution(Rm, g_star));
        return x0 + Vm.dot(y_opt);
    }

    Vector full_gmres_gauss(SparseCSR *A, const Vector &b, const Vector &x0, const double TOL, std::string out)
    {
        std::cout << "Beginning full GMRES with Gauß-Siedel preconditioning" << std::endl;
        std::ofstream datafile;
        datafile.open(out);
        datafile << "problem_size = " << b.length() << std::endl;
        datafile << "relative_tol = " << TOL << std::endl;

        SparseCSR E(A->E());
        Vector temp(b - A->dot(x0));
        std::cout << std::endl
                  << temp.norm() << std::endl;
        Vector r(precondition_via_forward_sub(E, temp));
        double rho_0 = r.norm();
        std::cout << "rho_0 = " << rho_0 << std::endl;
        datafile << "rho_0 = " << rho_0 << std::endl;

        Matrix krylov_basis(b.length(), FISS_MAX_KRYLOV_SIZE + 1);
        krylov_basis.set_col(0, r * (1 / rho_0));
        Matrix H_bar(FISS_MAX_KRYLOV_SIZE + 1, FISS_MAX_KRYLOV_SIZE);
        Vector c(FISS_MAX_KRYLOV_SIZE), s(FISS_MAX_KRYLOV_SIZE), g(FISS_MAX_KRYLOV_SIZE + 1);
        g[0] = rho_0;
        int j = 0;
        datafile << "residuals = [";
        std::cout << std::endl
                  << "beginning main loop" << std::endl;
        do
        {
            //extend krylov basis w/ jacobi precond
            Vector temp(A->dot(krylov_basis.get_col(j)));
            Vector w(precondition_via_forward_sub(E, temp));
            for (int i = 0; i <= j; i++)
            {
                VectorView v_i(krylov_basis.get_col(i));
                H_bar(i, j) = v_i.dot(w);
                w -= v_i * H_bar(i, j);
            }
            H_bar(j + 1, j) = w.norm();
            krylov_basis.set_col(j + 1, w * (1 / H_bar(j + 1, j)));
            // Apply all previous givens rotations
            double t1, t2, t3;
            for (int k = 0; k < j; k++)
            {
                t1 = c[k] * H_bar(k, j) + s[k] * H_bar(k + 1, j);
                t2 = -s[k] * H_bar(k, j) + c[k] * H_bar(k + 1, j);
                H_bar(k, j) = t1;
                H_bar(k + 1, j) = t2;
            }
            t3 = std::hypot(H_bar(j, j), H_bar(j + 1, j));
            c[j] = H_bar(j, j) / t3;
            s[j] = H_bar(j + 1, j) / t3;
            t1 = c[j] * H_bar(j, j) + s[j] * H_bar(j + 1, j);
            t2 = -s[j] * H_bar(j, j) + c[j] * H_bar(j + 1, j);
            H_bar(j, j) = t1;
            H_bar(j + 1, j) = t2;
            // pretty_print_matrix(H_bar);
            // Generate G_{j,j+1}
            g[j + 1] = -s[j] * g[j];
            g[j] = c[j] * g[j];
            datafile << std::abs(g[j + 1]) / rho_0 << ", ";
            j++;
            // limit of 600 just in case
        } while (std::abs(g[j] / rho_0) > TOL && j < 600);
        datafile << "]" << std::endl;
        datafile.close();

        MatrixView Rm(H_bar.submatrix(0, 0, j, j));
        MatrixView Vm(krylov_basis.submatrix(0, 0, b.length(), j));
        VectorView g_star(g.slice(0, j));
        Vector y_opt(solve_via_back_substitution(Rm, g_star));
        return x0 + Vm.dot(y_opt);
    }

    Vector conjugate_gradient(SparseLike *A, const Vector &b, const Vector &x0, const Vector &known_x, const double TOL, std::string out)
    {
        // mutable "current guess" vector
        Vector xm(x0);
        // mutable "current residual"
        Vector rm(b - A->dot(x0));
        Vector pm(rm);
        // constants
        double alpha, beta, rho_next;
        double rho = rm.norm();
        const double rho_0{rho};
        unsigned iter = 1;
        std::cout << "Finished CG setup. Current rel. residual is: " << rho / rho_0 << std::endl;
        std::ofstream datafile;
        datafile.open(out);

        datafile << "problem_size = " << b.length() << std::endl;
        datafile << "relative_tol = " << TOL << std::endl;
        datafile << "rho_0 = " << rho_0 << std::endl;
        datafile << "iter_data = [";

        auto start = std::chrono::high_resolution_clock::now();

        while (rho / rho_0 > TOL && iter < FISS_CG_MAX_ITER)
        {
            // cache this result, to avoid duplicating computations.
            Vector temp(A->dot(pm));
            alpha = rho * rho / temp.dot(pm);
            xm += pm * alpha;
            Vector rm_next(rm - (temp * alpha));
            rho_next = rm_next.norm();
            beta = rho_next * rho_next / (rho * rho);
            pm = rm_next + pm * beta;
            rm = rm_next;
            rho = rho_next;
            iter++;

            if (iter % 500 == 0)
            {
                auto end = std::chrono::high_resolution_clock::now();
                std::chrono::duration<double> elapsed = end - start;
                datafile << "{ 'iter':" << iter << ", 'duration':" << elapsed.count() << " ,'rho':" << rho;
                std::cout << "Iteration " << iter << " completed. Current rel. residual is: " << rho / rho_0 << std::endl;
                Vector e = known_x - xm;
                double e_A = std::sqrt(e.dot(A->dot(e)));
                datafile << ", 'err_A':" << e_A << "}," << std::endl;
                start = std::chrono::high_resolution_clock::now();
            }
        }
        // make sure we get the last data point
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double> elapsed = end - start;
        datafile << "{ 'iter':" << iter << ", 'duration':" << elapsed.count() << " ,'rho':" << rho;
        std::cout << "Iteration " << iter << " completed. Current rel. residual is: " << rho / rho_0 << std::endl;
        Vector e = known_x - xm;
        double e_A = std::sqrt(e.dot(A->dot(e)));
        datafile << ", 'err_A':" << e_A << "}]";
        datafile.close();

        return xm;
    }

}