#ifndef FISS_SPARSE_H
#define FISS_SPARSE_H

#include "vector.hpp"
#include "matrix.hpp"
namespace fiss
{

    class SparseCSR;
    class SparseLike
    {

    public:
        virtual Vector dot(const VectorLike &v) const = 0;
    };

    class SparseDiag : public SparseLike
    {
    private:
        unsigned m_dim;
        double *m_data;

    public:
        SparseDiag(unsigned dim, double *data);
        SparseDiag(SparseDiag &&s);
        ~SparseDiag();
        Vector dot(const VectorLike &v) const;
        SparseCSR dot(const SparseCSR &m) const;
        SparseDiag invert() const;
        double operator[](unsigned i) const;
    };

    

    class SparseCSR : public SparseLike
    {

    public:
        unsigned *m_row_start_idx;
        unsigned *m_col_idx;
        double *m_data;
        unsigned m_rows, m_cols;
        unsigned m_nonzero;

        //public:
        SparseCSR(unsigned t_rows, unsigned t_cols, unsigned t_nonzero, unsigned *t_row_start, unsigned *t_col_idx, double *t_data);
        SparseCSR(const SparseCSR &s);
        SparseCSR(SparseCSR &&s);
        ~SparseCSR();

        unsigned rows() const;
        unsigned cols() const;
        unsigned nonzero_elements() const;

        Vector diag() const;
        SparseCSR E() const;

        virtual Vector dot(const VectorLike &v) const;
    };

    SparseDiag extract_diagonal(const SparseCSR *m);

    class SparseCSRSymmetric : public SparseCSR
    {
    public:
        SparseCSRSymmetric(unsigned t_dim, unsigned t_nonzero, unsigned *t_row_start, unsigned *t_col_idx, double *t_data);
        SparseCSRSymmetric(const SparseCSRSymmetric &ss);
        SparseCSRSymmetric(SparseCSRSymmetric &&ss);
        ~SparseCSRSymmetric();

        Vector dot(const VectorLike &v) const;
    };

};

#endif